#!/bin/bash
n_iters=200
training_pool_id=1
adaptation_pool_id=1

# bert as meta-learner
# model_type="berttiny"
# python meta_main.py --operation=train --pool=./training_pool_${training_pool_id}.txt --model_dir=./model --data_path=../data-firm --bert
# python meta_main.py --operation=test --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-firm --checkpoint_path=./model/pretraining-metarl/metappo-${model_type}-ep${n_iters}.pth.tar --bert
# python meta_main.py --operation=adaptation --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-firm --checkpoint_path=./model/pretraining-metarl/metappo-${model_type}-ep${n_iters}.pth.tar --bert

# rnn as meta-learner
python meta_main.py --operation=train --pool=./training_pool_${training_pool_id}.txt --model_dir=./model --data_path=../data-firm
python meta_main.py --operation=test --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-firm --checkpoint_path=./model/pretraining-metarl/metappo-rnn-ep${n_iters}.pth.tar
python meta_main.py --operation=adaptation --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-firm --checkpoint_path=./model/pretraining-metarl/metappo-rnn-ep${n_iters}.pth.tar

# no meta-learner
# python main.py --operation=train --pool=./training_pool_${training_pool_id}.txt --model_dir=./model --data_path=../data-firm
# python main.py --operation=test --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-firm --checkpoint_path=./model/pretraining-rl/ppo-ep${n_iters}.pth.tar
# python main.py --operation=adaptation --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-firm --checkpoint_path=./model/pretraining-rl/ppo-ep${n_iters}.pth.tar
