import argparse
import matplotlib.pyplot as plt
import os

from serverless_env import SimEnvironment
from util import *

PLOT_FIG = True
SAVE_FIG = True
SAVE_TO_FILE = True

TOTAL_ITERATIONS = 100
EPISODES_PER_ITERATION = 5
EPISODE_LENGTH = 20
UTIL_THRESHOLD_LOWER = 0.5
UTIL_THRESHOLD_UPPER = 0.7

ALLOCATIONS = [128, 256, 512, 1024, 1536, 2048]
ALLOC_IDX = 2
UTIL_IDX = 0


def visualization(iteration_rewards, smoothed_rewards, data_path):
    plt.figure()
    plt.plot(iteration_rewards, color='steelblue', alpha=0.3)  # total rewards in an iteration or episode
    plt.plot(smoothed_rewards, color='steelblue')  # (moving avg) rewards
    plt.xlabel('Episodes')
    plt.ylabel('Total Reward per Episode')

    plt.tight_layout()
    if not SAVE_FIG:
        plt.show()
    else:
        plt.savefig(data_path + '/final.pdf')


class Policy:
    def __init__(self, env, function_name, data_path):
        self.env = env
        self.function_name = function_name
        self.data_path = data_path

        # for multi-agent control
        self.event = None
        self.training_task = None
        self.training_finished = False
        self.cpu_shares_other = 0
        self.token = False

    def run(self):
        # for plots
        iteration_rewards = []
        smoothed_rewards = []

        # for explainability
        iteration_slo_preservations = []
        smoothed_slo_preservations = []
        iteration_cpu_utils = []
        smoothed_cpu_utils = []

        for iteration in range(TOTAL_ITERATIONS):
            states = []
            rewards = []
            all_cpu_utils = []
            all_slo_preservations = []
            for episode in range(EPISODES_PER_ITERATION):
                state = self.env.reset(self.function_name)[:NUM_STATES]
                episode_rewards = []
                for step in range(EPISODE_LENGTH):
                    alloc = int(state[ALLOC_IDX] * ALLOCATIONS[-1])
                    alloc_idx = ALLOCATIONS.index(alloc)
                    util = state[UTIL_IDX]
                    if util < UTIL_THRESHOLD_LOWER:
                        # scale up
                        alloc_idx = min(len(ALLOCATIONS) - 1, alloc_idx + 1)
                    elif util > UTIL_THRESHOLD_UPPER:
                        # scale down
                        alloc_idx = max(0, alloc_idx - 1)
                    action_to_execute = {
                        'vertical': alloc_idx,
                        'horizontal': 0
                    }
                    next_state, reward, done = self.env.step(self.function_name, action_to_execute)
                    next_state = next_state[:NUM_STATES]
                    states.append(state)
                    episode_rewards.append(reward)
                    all_cpu_utils.append(next_state[0])
                    all_slo_preservations.append(next_state[1])
                    if done:
                        break
                    state = next_state
                # end of one episode
                rewards.append(episode_rewards)
            # end of one iteration
            iteration_rewards.append(np.mean([np.sum(episode_rewards) for episode_rewards in rewards]))
            smoothed_rewards.append(np.mean(iteration_rewards[-10:]))
            iteration_slo_preservations.append(np.mean(all_slo_preservations))
            smoothed_slo_preservations.append(np.mean(iteration_slo_preservations[-10:]))
            iteration_cpu_utils.append(np.mean(all_cpu_utils))
            smoothed_cpu_utils.append(np.mean(iteration_cpu_utils[-10:]))
            average_rewards = np.mean([np.sum(episode_rewards) for episode_rewards in rewards])

        # plot
        if PLOT_FIG:
            visualization(iteration_rewards, smoothed_rewards, self.data_path)
        # write rewards to file
        if SAVE_TO_FILE:
            print('Saving to files:', self.data_path)
            file = open(self.data_path + "/ppo_smoothed_rewards_static.txt", "w")
            for reward in smoothed_rewards:
                file.write(str(reward) + "\n")
            file.close()
            file = open(self.data_path + "/ppo_iteration_rewards_static.txt", "w")
            for reward in iteration_rewards:
                file.write(str(reward) + "\n")
            file.close()

            # write cpu_utils and slo_preservations to file
            file = open(self.data_path + "/ppo_cpu_utils_all_static.txt", "w")
            for cpu_util in iteration_cpu_utils:
                file.write(str(cpu_util) + "\n")
            file.close()
            file = open(self.data_path + "/ppo_cpu_utils_smoothed_static.txt", "w")
            for cpu_util in smoothed_cpu_utils:
                file.write(str(cpu_util) + "\n")
            file.close()
            file = open(self.data_path + "/ppo_slo_preservation_all_static.txt", "w")
            for ratio in iteration_slo_preservations:
                file.write(str(ratio) + "\n")
            file.close()
            file = open(self.data_path + "/ppo_slo_preservation_smoothed_static.txt", "w")
            for ratio in smoothed_slo_preservations:
                file.write(str(ratio) + "\n")
            file.close()


def main(data_path):
    env = SimEnvironment(data_path)
    function_name = env.get_function_name()
    print('Environment initialized for function', function_name)
    initial_state = env.reset(function_name)
    print('Initial state:', initial_state)

    # initialize the model folder path for the particular function
    folder_path = "./static/" + str(function_name)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    # init a static-policy agent
    agent = Policy(env, function_name, folder_path)
    print('Agent initialized!')
    agent.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run agent with static policies')
    parser.add_argument('--data_path', action='store', dest='data_path', type=str, help='Path to data file.')
    args = parser.parse_args()
    main(args.data_path)
