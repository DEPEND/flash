# RL-based Resource Management

RL-based resource management agent has been proposed and used for elastic applications (e.g., microservices deployed on Kubernetes and serverless applications on OpenWhisk).
See FIRM (OSDI 2020) and SIMPPO (SoCC 2022) for two such examples.

## Simulation

The RL model can trained by using simulation data generated from the traces located at `../data-firm/`.
The `../data-firm/` folder consists of the runtime traces for 1000 synthetic serverless applications collected in an OpenWhisk cluster.

## Usage - Training on One Application and Testing on One Application

Several input parameters are required from the main script `meta_main.py`.
- `operation`: `train`, `test`, or `adaptation`
- `model_dir`: The path to store the trained model checkpoint `.pth` file and auxiliary files
- `data_path`: The path to the training dataset or testing dataset
- `checkpoint_path`: The path to load the checkpoint to do the testing

To train the model on an application `compress_compress_decompress`:

```
python meta_main.py --operation=train --model_dir=./model --data_path=../data-firm/compress_compress_decompress_output.csv
```

Add `--bert` to replace the RNN embeddings with BERT-based embeddings.

To test the model on an application `compress_compress_decompress` (or any other application in `../data-firm/`):

```
python meta_main.py --operation=test --model_dir=./model --checkpoint_path=./model/compress_compress_decompress/<checkpoint>.pth.tar --data_path=../data-firm/compress_compress_decompress_output.csv
```

To adapt the model on an application `compress_compress_decompress` (or any other application in `../data-firm`):

```
python meta_main.py --operation=adaptation --model_dir=./model --checkpoint_path=./model/compress_compress_decompress/<checkpoint>.pth.tar --data_path=../data-firm/compress_compress_decompress_output.csv
```

## Usage - Training on a Pool of Applications and Testing on Another Pool

Several input parameters are required from the main script `meta_main.py`.
- `operation`: `train`, `test`, or `adaptation`
- `model_dir`: The path to store the trained model checkpoint `.pth` file and auxiliary files
- `data_path`: The path to the training dataset or testing dataset
- `checkpoint_path`: The path to load the checkpoint to do the testing

To train the model on a pool of applications `training_pool.txt`:

```
python meta_main.py --operation=train --pool=./training_pool.txt --model_dir=./model --data_path=../data-firm
```

Add `--bert` to replace the RNN embeddings with BERT-based embeddings. Output model checkpoint will be located at `./model/pretraining/`.

To test the model on a pool of applications `adaptation_pool_1.txt`:

```
python meta_main.py --operation=test --model_dir=./model --pool=./adaptation_pool_1.txt --checkpoint_path=./model/pretraining/<checkpoint>.pth.tar --data_path=../data-firm
```

To adapt the model on an application `compress_compress_decompress` (or any other application in `../data-firm`):

```
python meta_main.py --operation=adaptation --model_dir=./model --pool=./adaptation_pool_1.txt --checkpoint_path=./model/pretraining/<checkpoint>.pth.tar --data_path=../data-firm
```

## Static Policy

A widely used static policy for resource management is a threshold-based approach, i.e., resource scaling happens when the monitored metric is below or beyond a pre-determined threshold.
For example, if the observed CPU utilization is below 40%, then scaling down action is triggered; and vice versa.

```
python static.py --data_path=../data-firm/writefile_writefile_imageresize_output.csv 
```
