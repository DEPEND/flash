import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import transformers
from transformers import AutoConfig, AutoModel, BertModel, DataCollatorWithPadding

from blocks import *
from util import *


class RNNEmbedding(nn.Module):
    def __init__(self, N, K, task, device, num_channels=NUM_FEATURES_PER_SHOT, embedding_dim=EMBEDDING_DIM, verbose=True):
        # N-way (N classes, N = 1 for non-classification tasks), K-shot (K samples used to generate per embedding)
        super(RNNEmbedding, self).__init__()
        if task == 'wa':
            # num_channels is the dimension of x which is equal to the number of features per shot/sample
            num_channels = NUM_FEATURES_PER_SHOT
            if verbose:
                print('Task:', task)
                print('Number of features per sample:', num_channels)
            self.num_channels = num_channels
        else:
            raise ValueError('Not recognized task!')

        # configs for a 2-layer bi-directional RNN
        self.hidden_size = RNN_HIDDEN_SIZE
        self.num_layers = RNN_NUM_LAYERS
        self.bidirectional = True
        self.directions = 2 if self.bidirectional else 1
        self.gru = nn.GRU(num_channels, self.hidden_size, num_layers=self.num_layers, batch_first=True, bidirectional=self.bidirectional)

        # FC layer for embedding
        # self.embedding_layer = nn.ReLU(nn.Linear(self.hidden_size * self.num_layers * self.directions, embedding_dim))
        self.embedding_layer = nn.Linear(self.hidden_size * self.num_layers * self.directions, embedding_dim)
        self.relu = nn.ReLU()
        self.embedding_dim = embedding_dim

        self.N = N
        self.K = K
        self.device = device

    def init_hidden(self, num_sequences=1):
        return torch.zeros((self.directions * self.num_layers, num_sequences, self.hidden_size)).to(self.device)


class ResidualBlock(nn.Module):
    def __init__(self, input_size, hidden_size):
        super().__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, hidden_size)
        self.activation = nn.GELU()
    
    def forward(self, input):
        hidden = self.fc1(input)
        output = self.fc2(self.activation(hidden))
        # residual connection
        output += hidden
        return output


class BERTEmbedding(nn.Module):
    def __init__(self, N, K, task, num_channels=NUM_FEATURES_PER_SHOT, embedding_dim=EMBEDDING_DIM, verbose=True):
        # N-way (N classes, N = 1 for non-classification tasks), K-shot (K samples used to generate per embedding)
        super(BERTEmbedding, self).__init__()
        if task == 'wa':
            if verbose:
                print('Task:', task)
                print('Number of features per sample:', num_channels)
        else:
            raise ValueError('Not recognized task!')

        if FLAG_BERT_TINY:
            self.config = AutoConfig.from_pretrained('prajjwal1/bert-tiny')
        else:
            self.config = AutoConfig.from_pretrained('bert-base-uncased')
        self.bert =  AutoModel.from_config(self.config)
        self.encoder = ResidualBlock(num_channels, self.config.hidden_size)
        self.layer_norm = nn.LayerNorm(self.config.hidden_size)

        # FC layer for embedding
        self.embedding_layer = nn.Linear(self.config.hidden_size, embedding_dim)
        self.embedding_dim = embedding_dim

        self.N = N
        self.K = K

    # forward with embedding
    def forward(self, x):
        x = self.encoder(x)
        # x: [batch_size, seq_len, BERT_hidden_size]

        # Using inputs_embeds as input to BERT instead of input_ids.
        # Caution that we did not append the embeddings of the [CLS] and [SEP] tokens.
        x = self.bert(inputs_embeds=x)
        # Average over batch_size
        x = torch.mean(x.last_hidden_state, dim=0, keepdim=True)
        # Average over seq_len
        x = torch.mean(x, dim=1)
        x = self.embedding_layer(x.view(-1))
        return x


class SnailEmbedding(nn.Module):
    def __init__(self, N, K, task, use_cuda=False):
        # N-way (N classes, N = 1 for non-classification tasks), K-shot (K samples, default K = 1)
        super(RNNEmbedding, self).__init__()
        if task == 'wa':
            # num_channels is the dimension of x which is equal to the number of features per shot/sample
            num_channels = NUM_FEATURES_PER_SHOT
        else:
            raise ValueError('Not recognized task!')

        num_filters = int(math.ceil(math.log(N * K + 1, 2)))
        self.attention1 = AttentionBlock(num_channels, 64, 32)
        num_channels += 32
        self.tc1 = TCBlock(num_channels, N * K + 1, 128)
        num_channels += num_filters * 128
        self.attention2 = AttentionBlock(num_channels, 256, 128)
        num_channels += 128
        self.tc2 = TCBlock(num_channels, N * K + 1, 128)
        num_channels += num_filters * 128
        self.attention3 = AttentionBlock(num_channels, 512, 256)
        num_channels += 256

        self.fc1 = nn.Linear(num_channels, N)
        self.fc2 = nn.Linear(256, 64)
        self.fc3 = nn.Linear(64, N)
        self.relu = nn.ReLU()

        self.N = N
        self.K = K
        self.use_cuda = use_cuda

    # forward with embedding
    def forward(self, input):
        # input to the embedding layer is the features to the samples (excluding the config to predict for)
        x = np.array([input[0].numpy()[i * NUM_FEATURES_PER_SHOT : (i+1) * NUM_FEATURES_PER_SHOT] for i in range(self.K)])
        # # creating a tensor from a list of numpy.ndarrays is extremely slow -> convert the list to a single numpy.ndarray first
        x = torch.FloatTensor(x)
        x = x.view((1, self.K, -1))

        # apply the embedding layer
        x = self.attention1(x.float())
        x = self.tc1(x)
        x = self.attention2(x)
        x = self.tc2(x)
        x = self.attention3(x)
        # add a fully connected layer before passing the embedding
        # x = self.fc(x)

        # attach the generated embedding with the input
        x = x.view((1, 1, -1))
        input = input.view((1, 1, -1))
        x = torch.cat((x, input), 2)

        # append a fully connected neural network
        x = self.relu(self.fc1(x.float()))
        x = self.relu(self.fc2(x))
        x = self.fc3(x)
        return x



# deal with variable-sized trajectories
# one trajectory: { 'states': states_ep, 'actions': actions_ep, 'rewards': rewards_ep }
def get_padded_trajectories(trajectories_input, trajectory_rewards, state_dim=1, action_dim=1):
    batch_size = len(trajectory_rewards)
    if batch_size == 0:
        # where the buffer is empty
        max_traj_len = EPISODE_LENGTH
        batch_size = 1
        padded_states = torch.zeros(batch_size, state_dim, max_traj_len)
        padded_actions = torch.zeros(batch_size, 1, max_traj_len)
        padded_rewards = torch.zeros(batch_size, 1, max_traj_len)
        return padded_states, padded_actions, padded_rewards

    trajectories = []
    for traj_key in trajectories_input:
        if isinstance(trajectories_input[traj_key], list):
            for traj in trajectories_input[traj_key]:
                trajectories.append(traj)
        else:
            trajectories.append(trajectories_input[traj_key])
    max_traj_len = max([len(traj['states']) for traj in trajectories])
    # print('Maximum episode length:', max_traj_len)
    assert max_traj_len <= EPISODE_LENGTH
    max_traj_len = EPISODE_LENGTH

    # pad sequences in the batch to have equal length
    padded_states = torch.zeros(batch_size, state_dim, max_traj_len)
    # padded_actions = torch.zeros(batch_size, action_dim, max_traj_len)
    padded_actions = torch.zeros(batch_size, 1, max_traj_len)
    padded_rewards = torch.zeros(batch_size, 1, max_traj_len)
    for i, traj in enumerate(trajectories):
        for j in range(len(traj['states'])):
            for s in range(len(traj['states'][j])):
                padded_states[i][s][j] = torch.tensor(traj['states'][j][s])
            # for a in range(len(traj['actions'][j])):
            #     padded_actions[i][a][j] = torch.tensor(traj['actions'][j][a])
            padded_actions[i][0][j] = torch.tensor(traj['actions'][j])
            padded_rewards[i][0][j] = torch.tensor(traj['rewards'][j])

    return padded_states, padded_actions, padded_rewards


class MetaActorNetwork(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, env_dim, agent, verbose=True):
        super(MetaActorNetwork, self).__init__()

        self.env_dim = env_dim
        self.agent = agent
        self.device = self.agent.device

        if self.agent.bert_embedding:
            # bert embedding
            self.bert = BERTEmbedding(1, BUFFER_SIZE, 'wa', num_channels=NUM_FEATURES_PER_SHOT, verbose=verbose)
            self.fc1 = nn.Linear(input_size + self.bert.embedding_dim, hidden_size)
        else:
            # rnn embedding
            # num_features_per_sample = env_dim['state'] + env_dim['action'] + env_dim['reward']
            self.rnn = RNNEmbedding(1, BUFFER_SIZE, 'wa', self.agent.device, num_channels=NUM_FEATURES_PER_SHOT, verbose=verbose)
            self.fc1 = nn.Linear(input_size + self.rnn.embedding_dim, hidden_size)

        self.fc2 = nn.Linear(hidden_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, output_size)
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, input_):
        if self.agent.use_cached_embedding:
            # use cached embedding
            embedding = self.agent.cached_embedding
        else:
            # generate new embedding
            padded_states, padded_actions, padded_rewards = get_padded_trajectories(self.agent.episode_buffer, self.agent.episode_buffer_rewards, state_dim=self.env_dim['state'], action_dim=self.env_dim['action'])

            # encode RL trajectories with the RNN/BERT and generate the embedding
            embedding_input = torch.cat((padded_states, padded_actions, padded_rewards), dim=-2).to(self.device)
            embedding_input = embedding_input.reshape((-1, EPISODE_LENGTH, NUM_FEATURES_PER_SHOT))

            if self.agent.bert_embedding:
                embedding = self.bert(embedding_input)
            else:
                num_sequences = embedding_input.shape[0]
                hidden = self.rnn.init_hidden(num_sequences=num_sequences)
                rnn_output, hidden_state = self.rnn.gru(embedding_input, hidden)  # use the last hidden state to generate the embedding
                rnn_output = rnn_output[:, -1, :]
                hidden_state = torch.mean(hidden_state, dim=1, keepdim=True)
                hidden_state = hidden_state.view((1, 1, -1))
                # embedding = self.rnn.embedding_layer(torch.cat((rnn_output[:, :self.rnn.hidden_size], rnn_output[:, self.rnn.hidden_size:]), dim=-1))
                embedding = self.rnn.embedding_layer(hidden_state[0][0])
                embedding = self.rnn.relu(embedding)
                # print('Generated embedding:', type(embedding), embedding.shape)
            self.agent.cached_embedding = embedding
            self.use_cached_embedding = True

        if len(input_.shape) > 1:
            # concat input + embedding for batched inputs
            embedding = embedding.reshape(1, -1)
            embedding = embedding.repeat(input_.size(0), 1)
            input_ = torch.cat((input_, embedding), dim=1)
        else:
            # concat input + embedding
            input_ = torch.cat((input_, embedding), dim=-1)
        output = self.relu(self.fc1(input_))
        output = self.relu(self.fc2(output))
        output = self.fc3(output)
        if not FLAG_CONTINUOUS_ACTION:
            output = self.softmax(output)

        return output


class MetaCriticNetwork(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, env_dim, agent, verbose=True):
        super(MetaCriticNetwork, self).__init__()

        self.env_dim = env_dim
        self.agent = agent
        self.device = self.agent.device

        if self.agent.bert_embedding:
            # bert embedding
            self.bert = BERTEmbedding(1, BUFFER_SIZE, 'wa', num_channels=NUM_FEATURES_PER_SHOT, verbose=verbose)
            self.fc1 = nn.Linear(input_size + self.bert.embedding_dim, hidden_size)
        else:
            # rnn embedding
            # num_features_per_sample = env_dim['state'] + env_dim['action'] + env_dim['reward']
            self.rnn = RNNEmbedding(1, BUFFER_SIZE, 'wa', self.agent.device, num_channels=NUM_FEATURES_PER_SHOT, verbose=verbose)
            self.fc1 = nn.Linear(input_size + self.rnn.embedding_dim, hidden_size)

        self.fc2 = nn.Linear(hidden_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, output_size)
        self.relu = nn.ReLU()

    def forward(self, input_):
        if self.agent.use_cached_embedding:
            # use cached embedding
            embedding = self.agent.cached_embedding
        else:
            # generate new embedding
            padded_states, padded_actions, padded_rewards = get_padded_trajectories(self.agent.episode_buffer, self.agent.episode_buffer_rewards, state_dim=self.env_dim['state'], action_dim=self.env_dim['action'])

            # encode RL trajectories with the RNN/BERT and generate the embedding
            embedding_input = torch.cat((padded_states, padded_actions, padded_rewards), dim=-2).to(self.device)
            embedding_input = embedding_input.reshape((-1, RNN_HIDDEN_SIZE, NUM_FEATURES_PER_SHOT))
            
            if self.agent.bert_embedding:
                embedding = self.bert(embedding_input)
            else:
                num_sequences = embedding_input.shape[0]
                hidden = self.rnn.init_hidden(num_sequences=num_sequences)
                rnn_output, hidden_state = self.rnn.gru(embedding_input, hidden)  # use the last hidden state to generate the embedding
                rnn_output = rnn_output[:, -1, :]
                hidden_state = torch.mean(hidden_state, dim=1, keepdim=True)
                hidden_state = hidden_state.view((1, 1, -1))
                embedding = self.rnn.embedding_layer(hidden_state[0][0])
                embedding = self.rnn.relu(embedding)

            self.agent.cached_embedding = embedding
            self.use_cached_embedding = True

        # concat input + embedding
        embedding = embedding.reshape(1, -1)
        embedding = embedding.repeat(input_.size(0), 1)
        input_ = torch.cat((input_, embedding), dim=1)
        output = self.relu(self.fc1(input_))
        output = self.relu(self.fc2(output))
        output = self.fc3(output)

        return output
