import numpy as np
import matplotlib.pyplot as plt
import os


def read_rewards(filename):
    rewards = []
    with open(filename, 'r') as f:
        for line in f:
            try:
                rewards.append(float(line.strip()))
            except ValueError:
                pass
    return np.array(rewards[:-1])

def report_results():
    path = "./results-pool-1"
    pool_id = int(os.environ.get('POOL_ID_ENV', -1))
    if pool_id != -1:
        path = "./results-pool-" + str(pool_id)
    improved_reward_percentage = 0
    improved_adaptation_cost = 0

    cases = ['test', 'adapt']
    for case in cases:
        # read the rewards from the result files
        rl_rewards = read_rewards(path + '/' + case + '_rewards_rl.txt')
        rnn_rewards = read_rewards(path + '/' + case + '_rewards_rnn.txt')

        # calculate the statistics
        rl_p50 = np.percentile(rl_rewards, 50)
        rl_p90 = np.percentile(rl_rewards, 90)
        rl_p99 = np.percentile(rl_rewards, 99)
        rl_avg = np.mean(rl_rewards)

        rnn_p50 = np.percentile(rnn_rewards, 50)
        rnn_p90 = np.percentile(rnn_rewards, 90)
        rnn_p99 = np.percentile(rnn_rewards, 99)
        rnn_avg = np.mean(rnn_rewards)

        improved_reward_percentage = (rnn_avg - rl_avg) / 50 * 100
        rl_adapt_cost = np.square((50 - rl_avg) / 0.2)
        rnn_adapt_cost = np.square((50 - rnn_avg) / 0.2)
        improved_adaptation_cost = rl_adapt_cost / rnn_adapt_cost

        # plot the CDF
        plt.figure(figsize=(5, 3))
        sorted_rl_rewards = np.sort(rl_rewards)
        sorted_rnn_rewards = np.sort(rnn_rewards)
        reward_values_rl, counts_rl = np.unique(sorted_rl_rewards, return_counts=True)
        reward_values_rnn, counts_rnn = np.unique(sorted_rnn_rewards, return_counts=True)
        cdf_rl = np.cumsum(counts_rl) / len(rl_rewards)
        cdf_rnn = np.cumsum(counts_rnn) / len(rnn_rewards)
        plt.plot(reward_values_rl, cdf_rl, label='RL')
        plt.plot(reward_values_rnn, cdf_rnn, label='RNN')
        plt.xlabel('Rewards per Episode')
        plt.ylabel('CDF')
        # plt.title('Cumulative Distribution Function (CDF) of Rewards')
        plt.legend()
        plt.tight_layout()
        plt.savefig(path + '/' + case + '.pdf')

        # Print the statistics
        print(f"RL: p50={rl_p50:.2f}, p90={rl_p90:.2f}, p99={rl_p99:.2f}, avg={rl_avg:.2f}")
        print(f"RNN: p50={rnn_p50:.2f}, p90={rnn_p90:.2f}, p99={rnn_p99:.2f}, avg={rnn_avg:.2f}")
    print("Improved reward percentage:", str(round(improved_reward_percentage, 2)) + '%')
    print("Improved adaptation cost:", str(round(improved_adaptation_cost, 2)) + 'x')
    return


if __name__ == "__main__":
    report_results()
