# Case Study 1: Resource Config Search for OpenWhisk

Category: Meta-learning for Supervised Learning in Regression Tasks

## OpenWhisk Dataset

Location: `../training-data-owk/processed-data.csv`

This dataset consists of the runtime resource metrics and appilcation metrics for 1000 serverless functions deployed on a 4-node OpenWhisk cluster.
There are 36 different combinations of resource configurations (`cpu.shares`: 128, 256, 512, 1024, 1536, 2048; memory (MB): 128, 256, 512, 1024, 1536, 2048).
The total compute time is around 60 days on IBM Cloud worthy ~$4700.

## Requirement

- Python 3.8.8
- Packages listed in `requirements.txt`

## Training

To start training:

```
# embedding + fully-connected neural network -> latency
python3 train.py --dataset=openwhisk                # default: 2-shots/samples
python3 train.py --dataset=openwhisk --num_shots=6  # 6-shots/samples

# embedding + sizeless neural network architecture -> latency
python3 train.py --dataset=openwhisk --sizeless --decay=0.01

# embedding + fully-connected neural network (with L2 regularization) -> latency
python3 train.py --dataset=openwhisk --decay=0.01

# embedding + sizeless neural network architecture (without L2 regularization) -> latency
python3 train.py --dataset=openwhisk --sizeless

# RNN embedding (with 4 samples) + fully-connected neural network -> latency
python3 train.py --dataset=openwhisk --rnn --num_shots=4
```

## Evaluation

To evaluated the trained model on a new dataset with the same other flags used in the training:

```
# use --exp to specify the folder where the trained model is located at
python3 eval.py [--dataset=openwhisk --rnn --num_shots=4] --exp=experiment-name
```

## Visualization

To visualize the training process:

```
# use --exp to specify the folder where the training loss log data is located at
python3 plot_training_loss.py --exp=experiment-name
```

## Example

```
# Sizeless
$ python3 train.py --dataset=openwhisk --skip_embedding --num_shots=2 --exp=sizeless-2s-100ep-exp0
=== Epoch: 0 ===
100%|████████████████████████████████████████████████████████████████████████████| 18795/18795 [01:04<00:00, 293.40it/s]
...
...
=== Epoch: 99 ===
100%|████████████████████████████████████████████████████████████████████████████| 18795/18795 [01:02<00:00, 301.89it/s]
Avg Train Loss: 0.04025072909455116, Avg Train MAPE: 0.16798052554697282
Avg Val Loss: 0.03833355645579362, Avg Val MAPE: 0.21020086597298432 (Best: 0.17050846777106063)
Testing with last model..
Test Avg MAPE: 0.86481474919530923
Testing with best model..
Test Avg MAPE: 0.86481474919530923

# Meta-learning
$ python3 train.py --dataset=openwhisk --rnn --num_shots=6 --exp=rnn-6s-100ep-exp0
=== Epoch: 0 ===
100%|█████████████████████████████████████████████████████████████████████████████| 17689/17689 [04:25<00:00, 66.74it/s]
...
...
=== Epoch: 99 ===
100%|█████████████████████████████████████████████████████████████████████████████| 16584/16584 [04:14<00:00, 65.12it/s]
Avg Train Loss: 0.031487790207150285, Avg Train MAPE: 0.2096543876127326
Avg Train Loss: 0.031487790207150285, Avg Train MAPE: 0.2096543876127326
Avg Val Loss: 0.04677456660967707, Avg Val MAPE: 0.19799323232237412 (Best: 0.18637619851368728)
Testing with last model..
Test Avg MAPE: 0.21739989436364995
Testing with best model..
Test Avg MAPE: 0.21739989436364995
```
