# FLASH + sizeless agent
printf ">>> FLASH + Sizeless Agent\n"
printf "\n1-shot testing:\n"
python3 eval.py --dataset=openwhisk --rnn --num_shots=1 --exp=rnn-1s-100ep-exp0

printf "\n2-shot testing:\n"
python3 eval.py --dataset=openwhisk --rnn --num_shots=2 --exp=rnn-2s-100ep-exp0

printf "\n3-shot testing:\n"
python3 eval.py --dataset=openwhisk --rnn --num_shots=3 --exp=rnn-3s-100ep-exp0

# sizeless agent
printf "\n>>> Sizeless Agent (No Meta-Learning)\n"
printf "\n1-shot testing:\n"
python3 eval.py --dataset=openwhisk --num_shots=1 --exp=sizeless-1s-100ep-test --skip_embedding

printf "\n2-shot testing:\n"
python3 eval.py --dataset=openwhisk --num_shots=2 --exp=sizeless-2s-100ep-test --skip_embedding

printf "\n3-shot testing:\n"
python3 eval.py --dataset=openwhisk --num_shots=3 --exp=sizeless-3s-100ep-test --skip_embedding

printf "\nDone!\n"