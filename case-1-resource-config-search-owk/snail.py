import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from blocks import *
from params import *


class SnailFewShot(nn.Module):
    def __init__(self, N, K, task, use_cuda=False):
        # N-way (N classes, N = 1 for regression tasks), K-shot (K samples, default K = 1)
        super(SnailFewShot, self).__init__()
        if task == 'sizeless' or task == 'multicloud' or task == 'openwhisk':
            self.encoder = FullyConnectedNet()
            # num_channels is the dimension of x which is equal to the number of features per shot/sample
            num_channels = NUM_FEATURES_PER_SHOT  # 8 (if self.encoder is applied to input)
        else:
            raise ValueError('Not recognized task!')

        num_filters = int(math.ceil(math.log(N * K + 1, 2)))
        self.attention1 = AttentionBlock(num_channels, 64, 32)
        num_channels += 32
        self.tc1 = TCBlock(num_channels, N * K + 1, 128)
        num_channels += num_filters * 128
        self.attention2 = AttentionBlock(num_channels, 256, 128)
        num_channels += 128
        self.tc2 = TCBlock(num_channels, N * K + 1, 128)
        num_channels += num_filters * 128
        self.attention3 = AttentionBlock(num_channels, 512, 256)
        num_channels += 256

        self.fc = nn.Linear(num_channels, N)
        if FEED_BOTH_SAMPLE_FEATURES_AND_CONFIGS_TO_PREDICT:
            self.fc1 = nn.Linear(num_channels * K + NUM_FEATURES_PER_SHOT * K + NUM_CONFIG_PARAMS, 256)
        else:
            self.fc1 = nn.Linear(num_channels * K + NUM_CONFIG_PARAMS, 256)
        self.fc2 = nn.Linear(256, 64)
        self.fc3 = nn.Linear(64, N)
        self.relu = nn.ReLU()

        self.N = N
        self.K = K
        self.use_cuda = use_cuda

    # embedding + fully-connected layer
    def forward(self, input, labels):
        # x = self.encoder(input)
        # input to the embedding layer is the features to the samples (excluding the config to predict for)
        x = np.array([input[0].numpy()[i * NUM_FEATURES_PER_SHOT : (i+1) * NUM_FEATURES_PER_SHOT] for i in range(self.K)])
        x = torch.FloatTensor(x)  # Creating a tensor from a list of numpy.ndarrays is extremely slow. Convert the list to a single numpy.ndarray first.
        x = x.view((1, self.K, -1))

        # apply the embedding layer
        x = self.attention1(x.float())
        x = self.tc1(x)
        x = self.attention2(x)
        x = self.tc2(x)
        x = self.attention3(x)
        # add a fully connected layer before passing the embedding
        # x = self.fc(x)

        if FEED_BOTH_SAMPLE_FEATURES_AND_CONFIGS_TO_PREDICT:
            # attach the generated embedding with the input  -- option #1
            x = x.view((1, 1, -1))
            input = input.view((1, 1, -1))
            x = torch.cat((x, input), 2)
        else:
            # attach the generated embedding with the config to predict for  -- option #2
            x = x.view((1, 1, -1))
            x = torch.cat((x, torch.tensor(np.array([[input[0].numpy()[-NUM_CONFIG_PARAMS:]]]))), 2)

        # go through a fully connected neural network
        x = self.relu(self.fc1(x.float()))
        x = self.relu(self.fc2(x))
        x = self.fc3(x)
        return x

class SnailFewShotOnSizeless(nn.Module):
    def __init__(self, N, K, task, use_cuda=False):
        super(SnailFewShotOnSizeless, self).__init__()
        if task == 'sizeless' or task == 'multicloud' or task == 'openwhisk':
            # num_channels is the dimension of x which is equal to the number of features per shot/sample
            num_channels = NUM_FEATURES_PER_SHOT
        else:
            raise ValueError('Not recognized task!')

        num_filters = int(math.ceil(math.log(N * K + 1, 2)))
        self.attention1 = AttentionBlock(num_channels, 64, 32)
        num_channels += 32
        self.tc1 = TCBlock(num_channels, N * K + 1, 128)
        num_channels += num_filters * 128
        self.attention2 = AttentionBlock(num_channels, 256, 128)
        num_channels += 128
        self.tc2 = TCBlock(num_channels, N * K + 1, 128)
        num_channels += num_filters * 128
        self.attention3 = AttentionBlock(num_channels, 512, 256)
        num_channels += 256

        if FEED_BOTH_SAMPLE_FEATURES_AND_CONFIGS_TO_PREDICT:
            self.fc1 = nn.Linear(num_channels * K + NUM_FEATURES_PER_SHOT * K + NUM_CONFIG_PARAMS, 256)
        else:
            self.fc1 = nn.Linear(num_channels * K + NUM_CONFIG_PARAMS, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 256)
        self.fc4 = nn.Linear(256, 256)
        self.fc5 = nn.Linear(256, N)
        self.relu = nn.ReLU()

        self.N = N
        self.K = K
        self.use_cuda = use_cuda

    # embedding + fully-connected layer (Sizeless architecture)
    def forward(self, input, labels):
        # input to the embedding layer is the features to the samples (excluding the config to predict for)
        x = torch.tensor([input[0].numpy()[i * NUM_FEATURES_PER_SHOT : (i+1) * NUM_FEATURES_PER_SHOT] for i in range(self.K)])
        x = x.view((1, self.K, -1))

        # apply the embedding layer
        x = self.attention1(x.float())
        x = self.tc1(x)
        x = self.attention2(x)
        x = self.tc2(x)
        x = self.attention3(x)

        if FEED_BOTH_SAMPLE_FEATURES_AND_CONFIGS_TO_PREDICT:
            # attach the generated embedding with the input  -- option #1
            x = x.view((1, 1, -1))
            input = input.view((1, 1, -1))
            x = torch.cat((x, input), 2)
        else:
            # attach the generated embedding with the config to predict for  -- option #2
            x = x.view((1, 1, -1))
            x = torch.cat((x, torch.tensor(np.array([[input[0].numpy()[-NUM_CONFIG_PARAMS:]]]))), 2)

        # go through a fully connected neural network (Sizeless architecture)
        x = self.relu(self.fc1(x.float()))
        x = self.relu(self.fc2(x))
        x = self.relu(self.fc3(x))
        x = self.relu(self.fc4(x))
        x = self.fc5(x)
        return x

class Sizeless(nn.Module):
    def __init__(self, num_features=NUM_FEATURES_PER_SHOT + 1):
        super(Sizeless, self).__init__()
        self.fc1 = nn.Linear(num_features, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 256)
        self.fc4 = nn.Linear(256, 256)
        self.fc5 = nn.Linear(256, 1)
        self.relu = nn.ReLU()

    # fully-connected layers
    def forward(self, input, labels):
        input = input.view((1, 1, -1))
        x = self.relu(self.fc1(input.float()))
        x = self.relu(self.fc2(x))
        x = self.relu(self.fc3(x))
        x = self.relu(self.fc4(x))
        x = self.fc5(x)
        return x


class RNNFewShot(nn.Module):
    def __init__(self, N, K, task, use_cuda=False):
        # N-way (N classes, N = 1 for regression tasks), K-shot (K samples, default K = 1)
        super(RNNFewShot, self).__init__()
        
        if task == 'sizeless' or task == 'multicloud' or task == 'openwhisk':
            self.encoder = FullyConnectedNet()
            # num_channels is the dimension of x which is equal to the number of features per shot/sample
            num_channels = NUM_FEATURES_PER_SHOT  # 8 (if self.encoder is applied to input)
        else:
            raise ValueError('Not recognized task!')

        self.hidden_size = 256
        self.num_layers = 2     # stack 2 RNN layers
        self.bidirectional = True
        self.directions = 2 if self.bidirectional else 1     # use bi-directional RNNs
        self.gru = nn.GRU(num_channels, self.hidden_size, num_layers=self.num_layers, batch_first=True, bidirectional=self.bidirectional)


        if FEED_BOTH_SAMPLE_FEATURES_AND_CONFIGS_TO_PREDICT:
            self.fc1 = nn.Linear(self.directions * self.num_layers * self.hidden_size + NUM_FEATURES_PER_SHOT * K + NUM_CONFIG_PARAMS, 256)
        else:
            self.fc1 = nn.Linear(self.directions * self.num_layers * self.hidden_size + NUM_CONFIG_PARAMS, 256)
        self.fc2 = nn.Linear(256, 64)
        self.fc3 = nn.Linear(64, N)
        self.relu = nn.ReLU()

        self.N = N
        self.K = K
        self.use_cuda = use_cuda


    # embedding + fully-connected layer
    def forward(self, input, labels):
        # x = self.encoder(input)
        # input to the embedding layer is the features to the samples (excluding the config to predict for)
        x = np.array([input[0].numpy()[i * NUM_FEATURES_PER_SHOT : (i+1) * NUM_FEATURES_PER_SHOT] for i in range(self.K)])
        x = torch.FloatTensor(x)   # Creating a tensor from a list of numpy.ndarrays is extremely slow. Convert the list to a single numpy.ndarray first. 
        x = x.view((1, self.K, -1))
        hidden = self.init_hidden()

        output, x = self.gru(x, hidden)     # use the last hidden state as the embedding

        if FEED_BOTH_SAMPLE_FEATURES_AND_CONFIGS_TO_PREDICT:
            # attach the generated embedding with the input  -- option #1
            x = x.view((1, 1, -1))
            input = input.view((1, 1, -1))
            x = torch.cat((x, input), 2)
        else:
            # attach the generated embedding with the config to predict for  -- option #2
            x = x.view((1, 1, -1))
            x = torch.cat((x, torch.tensor(np.array([[input[0].numpy()[-NUM_CONFIG_PARAMS:]]]))), 2)

        # go through a fully connected neural network
        x = self.relu(self.fc1(x.float()))
        x = self.relu(self.fc2(x))
        x = self.fc3(x)
        return x


    def init_hidden(self):
        return torch.zeros((self.directions * self.num_layers, 1, self.hidden_size))