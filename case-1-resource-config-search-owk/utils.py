from owk_dataset import *
from torch.utils.data.sampler import SubsetRandomSampler
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader


def init_dataset(opt, validation_only=False):
    '''
    Initialize the datasets, samplers and dataloaders
    '''
    # if opt.dataset == 'sizeless':
    #     if validation_only:
    #         val_dataset = SizelessDatasetXSamplesDifferentAppsForTrainTestValidate(mode='test', num_samples=opt.num_shots)
    #         trainval_dataloader = DataLoader(val_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(range(val_dataset.x_test.shape[0])))
    #         return trainval_dataloader
    #     else:
    #         # train : validate : test = 0.56 : 0.14 : 0.3
    #         # train + validate = 0.7, test = 0.3
    #         train_dataset = SizelessDatasetXSamplesDifferentAppsForTrainTestValidate(mode='train', num_samples=opt.num_shots)
    #         val_dataset = SizelessDatasetXSamplesDifferentAppsForTrainTestValidate(mode='test', num_samples=opt.num_shots)

    #         # train 0.7*0.8, validate 0.7*0.2
    #         train, validate = train_test_split(list(range(train_dataset.x_train.shape[0])), test_size=.2)

    #         tr_dataloader = DataLoader(train_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(train))
    #         val_dataloader = DataLoader(train_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(validate))
    #         test_dataloader = DataLoader(val_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(range(val_dataset.x_test.shape[0])))
    #         return tr_dataloader, val_dataloader, test_dataloader, None
    # elif opt.dataset == 'multicloud':
    #     if validation_only:
    #         val_dataset = MultiCloudConfigSearchDatasetXSamples(mode='test', num_samples=opt.num_shots)
    #         trainval_dataloader = DataLoader(val_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(range(val_dataset.x_test.shape[0])))
    #         return trainval_dataloader
    #     else:
    #         # train : validate : test = 0.56 : 0.14 : 0.3
    #         # train + validate = 0.7, test = 0.3
    #         train_dataset = MultiCloudConfigSearchDatasetXSamples(mode='train', num_samples=opt.num_shots)
    #         val_dataset = MultiCloudConfigSearchDatasetXSamples(mode='test', num_samples=opt.num_shots)

    #         # train 0.7*0.8, validate 0.7*0.2
    #         train, validate = train_test_split(list(range(train_dataset.x_train.shape[0])), test_size=.2)

    #         tr_dataloader = DataLoader(train_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(train))
    #         val_dataloader = DataLoader(train_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(validate))
    #         test_dataloader = DataLoader(val_dataset, batch_size=opt.batch_size,
    #                                sampler=SubsetRandomSampler(range(val_dataset.x_test.shape[0])))
    #         return tr_dataloader, val_dataloader, test_dataloader, None
    if opt.dataset == 'openwhisk':
        if validation_only:
            val_dataset = OWKDatasetXSamplesDifferentAppsForTrainTestValidate(mode='test', num_samples=opt.num_shots)
            trainval_dataloader = DataLoader(val_dataset, batch_size=opt.batch_size,
                                   sampler=SubsetRandomSampler(range(val_dataset.x_test.shape[0])))
            return trainval_dataloader
        else:
            # train : validate : test = 0.56 : 0.14 : 0.3
            # train + validate = 0.7, test = 0.3
            train_dataset = OWKDatasetXSamplesDifferentAppsForTrainTestValidate(mode='train', num_samples=opt.num_shots)
            val_dataset = OWKDatasetXSamplesDifferentAppsForTrainTestValidate(mode='test', num_samples=opt.num_shots)

            # train 0.7*0.8, validate 0.7*0.2
            train, validate = train_test_split(list(range(train_dataset.x_train.shape[0])), test_size=.2)

            tr_dataloader = DataLoader(train_dataset, batch_size=opt.batch_size,
                                   sampler=SubsetRandomSampler(train))
            val_dataloader = DataLoader(train_dataset, batch_size=opt.batch_size,
                                   sampler=SubsetRandomSampler(validate))
            test_dataloader = DataLoader(val_dataset, batch_size=opt.batch_size,
                                   sampler=SubsetRandomSampler(range(val_dataset.x_test.shape[0])))
            return tr_dataloader, val_dataloader, test_dataloader, None
    else:
        raise ValueError('Not recognized task!')