import torch.utils.data as data
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer
import pandas as pd
import itertools
import numpy as np
from params import *
import math


# functions with missing measurements or malfunctioning
excludes = ['imageresize_matmul_readfile', 'matmul_imagecompress_imagecompress', 'matmul_imagecompress_readfile', 'matmul_imageresize_readfile', 'matmul_imagerotate_readfile', 'matmul_readfile_imagecompress', 'matmul_readfile_imageresize', 'matmul_readfile_imagerotate', 'readfile_imageresize_matmul', 'readfile_imagerotate_matmul', 'readfile_matmul_imageresize', 'readfile_matmul_imagerotate', 'writefile_imagecompress_matmul']
# variables with missing measurements or malfunctioning
excludes_features = []

used_features = ['cpu', 'memory', 'userDiff_mean', 'sysDiff_mean', 'heapUsed_mean', 'rel_vContextSwitches_mean', 'rel_userdiff_mean', 'rel_sysdiff_mean', 'rel_fsWrite_mean',  'rel_netByRx_mean', 'heapUsed_cov', 'mallocMem_cov', 'rel_userdiff_cov']
# all features with fields containing NaN values removed
if USE_ALL_FEATURES:
    used_features = 'cpu,memory,maxRss_mean,fsRead_mean,fsWrite_mean,vContextSwitches_mean,ivContextSwitches_mean,userDiff_mean,sysDiff_mean,rss_mean,heapTotal_mean,heapUsed_mean,external_mean,elMin_mean,elMax_mean,elMean_mean,elStd_mean,bytecodeMetadataSize_mean,heapPhysical_mean,heapAvailable_mean,heapLimit_mean,mallocMem_mean,netByRx_mean,netPkgRx_mean,netByTx_mean,netPkgTx_mean,maxRss_std,fsRead_std,fsWrite_std,vContextSwitches_std,ivContextSwitches_std,userDiff_std,sysDiff_std,rss_std,heapTotal_std,heapUsed_std,external_std,elMin_std,elMax_std,elMean_std,elStd_std,bytecodeMetadataSize_std,heapPhysical_std,heapAvailable_std,heapLimit_std,mallocMem_std,netByRx_std,netPkgRx_std,netByTx_std,netPkgTx_std'.split(',')

memory_configs = ['128', '256', '512', '1024', '1536', '2048']
cpu_configs = ['128', '256', '512', '1024', '1536', '2048']

# normalize the latency value to [0, 1]; the base is the latency under (128 cpu.shares, 128 MB)
def normalize_latency_X_samples(dataset, num_samples):
    dataset['duration_target'] = dataset['duration_target'] / dataset['duration_s1']

    for i in reversed(range(num_samples)):
        if 'duration_s' + str(i + 1) in dataset.columns:
            dataset['duration_s' + str(i + 1)] = dataset['duration_s' + str(i + 1)] / dataset['duration_s1']
    return dataset

# normalize the configs to [0, 1]; the base is (2048 cpu.shares, 2048 MB)
def normalize_function_configs(dataset, num_samples, min_config=128.0, max_config=2048.0):
    dataset['cpu'] /= max_config
    dataset['memory'] /= max_config

    for i in range(num_samples):
        if 'cpu_s' + str(i + 1) in dataset.columns:
            dataset['cpu_s' + str(i + 1)] /= max_config
        if 'memory_s' + str(i + 1) in dataset.columns:
            dataset['memory_s' + str(i + 1)] /= max_config
    return dataset

# owk dataset that supports X-sample dataset generation with separated applications (X-sample -> remaining samples)
# for training and validation to test if the learned model is generalizable or adaptable to unseen applications
class OWKDatasetXSamplesDifferentAppsForTrainTestValidate(data.Dataset):
    def __init__(self, num_samples, mode='train', root='../training-data-owk', transform=None, target_transform=None):
        '''
        Args:
        - num_sammples: number of samples used as features, remaining ones are used for prediction
        - mode: one of ['train', 'test']
        - root: the directory where the dataset is stored
        - transform: how to transform the input
        - target_transform: how to transform the target
        '''
        super(OWKDatasetXSamplesDifferentAppsForTrainTestValidate, self).__init__()
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.mode = mode

        if num_samples > 6:
            print("Error! Maximum number of samples to use in the OpenWhisk dataset is 6!")
            exit()

        training_data_df = pd.read_csv(self.root + '/processed-data.csv').reset_index(drop=True)

        # drop malfunctioned measurements
        self.training_data_df = training_data_df[~training_data_df.f_name.isin(excludes)]
        self.training_data_df = self.training_data_df.drop(excludes_features, axis=1)

        # group by function name
        training_data_df_grouped = self.training_data_df.groupby('f_name')

        all_cols = []

        # build the sample cols
        for i in range(num_samples):
            for col_name in used_features:
                col_name += '_s' + str(i + 1)
                all_cols.append(col_name)
            all_cols.append('duration_s' + str(i + 1))
            # all_cols.append('duration_std_s' + str(i + 1))

        # build the target cols
        all_cols.append('cpu')
        all_cols.append('memory')
        all_cols.append('duration_target')
        print('Total number of columns:', len(all_cols))

        # init a new dataframe
        processed_df = pd.DataFrame(columns=all_cols)

        count_functions = 0
        for group_name, df_group in training_data_df_grouped:
            # print('f_name:', group_name)
            count = 0
            row_to_add = []
            for row_index, row in df_group.sort_values(['cpu', 'memory']).iterrows():
                if num_samples <= count:
                    # targets to predict
                    processed_df.loc[len(processed_df.index)] = row_to_add + [row['cpu'], row['memory'], row['duration_mean']]
                else:
                    # samples to be used as features
                    row_to_add.extend(row[used_features])
                    row_to_add.append(row['duration_mean'])
                    # row_to_add.append(row['duration_std'])
                    # check if nan exists
                    if any(np.isnan(row_to_add)):
                        print('Measurements data contains NaN:', group_name)
                        exit()
                count += 1
            count_functions += 1
            # if count_functions >= 200:
            #     break

        print('Total # of functions for training and validation (7:3):', count_functions)

        # normalize latency values and function configs
        self.training_data_df = normalize_latency_X_samples(processed_df, num_samples)
        self.training_data_df = normalize_function_configs(self.training_data_df, num_samples)
        # self.training_data_df.to_csv('test-dataset-normalized.csv')

        # split the labels Y
        self.y = [elem for elem in self.training_data_df['duration_target']]
        training_dataset_end_index = math.ceil(0.7 * count_functions) * (TOTAL_NUM_CONFIGS - num_samples)
        self.y_train = self.y[:training_dataset_end_index]
        self.y_test = self.y[training_dataset_end_index:]

        # extract the features X
        cols_cpu_configs = [col for col in self.training_data_df.columns if 'cpu' in col]
        cols_memory_configs = [col for col in self.training_data_df.columns if 'memory' in col]
        cols_durations = [col for col in self.training_data_df.columns if 'duration' in col and not 'target' in col]
        self.x = self.training_data_df.drop(self.training_data_df.columns.difference(all_cols[:-1]), axis=1)
        self.x_cpu_configs = self.x[cols_cpu_configs]
        self.x_memory_configs = self.x[cols_memory_configs]
        self.x_durations = self.x[cols_durations]
        self.x_others = self.x.drop(cols_cpu_configs + cols_memory_configs + cols_durations, axis=1)

        # normalize features
        sc2 = QuantileTransformer(n_quantiles=min(1000, len(self.x_others)))
        self.x_others = sc2.fit_transform(self.x_others)
        sc = MinMaxScaler(clip=True)
        self.x_others = sc.fit_transform(self.x_others)

        self.x_cpu_configs = self.x_cpu_configs.to_numpy()
        self.x_memory_configs = self.x_memory_configs.to_numpy()
        self.x_durations = self.x_durations.to_numpy()
        # insert the config and duration columns to the end of each sample feature vector in x_others
        # e.g., for 2 samples: a1, a2, a3, b1, b2, b3 | (c_a, m_a, t_a), (c_b, m_b, t_b)
        # -> a1, a2, a3, c_a, m_a, t_a, b1, b2, b3 | (c_b, m_b, t_b)
        # -> a1, a2, a3, c_a, m_a, t_a, b1, b2, b3, c_b, m_b, t_b
        for i in range(num_samples):
            self.x_others = np.insert(self.x_others, (i + 1) * (len(used_features) - 2) + 3 * i, self.x_cpu_configs[:, i], axis=1)
            self.x_others = np.insert(self.x_others, (i + 1) * (len(used_features) - 2) + 3 * i + 1, self.x_memory_configs[:, i], axis=1)
            self.x_others = np.insert(self.x_others, (i + 1) * (len(used_features) - 2) + 3 * i + 2, self.x_durations[:, i], axis=1)
        self.x_cpu_configs = np.delete(self.x_cpu_configs, list(range(num_samples)), 1)
        self.x_memory_configs = np.delete(self.x_memory_configs, list(range(num_samples)), 1)
        self.x = np.concatenate((self.x_others, self.x_cpu_configs), axis=1)
        self.x = np.concatenate((self.x, self.x_memory_configs), axis=1)

        # normalize features
        # self.x = self.training_data_df.drop(self.training_data_df.columns.difference(all_cols[:-1]), axis=1)
        # sc2 = QuantileTransformer(n_quantiles=min(1000, len(self.x)))
        # self.x = sc2.fit_transform(self.x)
        # sc = MinMaxScaler(clip=True)
        # self.x = sc.fit_transform(self.x)

        # split the features X
        self.x_train = self.x[:training_dataset_end_index, :]
        self.x_test = self.x[training_dataset_end_index:, :]

    def __getitem__(self, idx):
        if self.mode == 'train':
            x = self.x_train[idx]
            if self.transform:
                x = self.transform(x)
            return x, self.y_train[idx]
        elif self.mode == 'test':
            x = self.x_test[idx]
            if self.transform:
                x = self.transform(x)
            return x, self.y_test[idx]

    def __len__(self):
        if self.mode == 'train':
            return len(self.y_train)
        elif self.mode =='test':
            return len(self.y_test)


if __name__ == '__main__':
    # testing
    train_dataset = OWKDatasetXSamplesDifferentAppsForTrainTestValidate(4, mode = 'train')
