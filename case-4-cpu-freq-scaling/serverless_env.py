import pandas as pd
import random
from util import *


class SimEnvironment:
    # initial states
    initial_arrival_rate = 1
    initial_cpu_shares = 2048
    initial_frequency = 2
    initial_cpu_shares_others = 0
    memory = 256
    cpu_dictionary = {0:128, 1:256, 2:512, 3:1024, 4:1536, 5:2048}
    frequency_dictionary = {0:1, 1:1.5, 2:2, 3:2.5, 4:3}
    # tabular data
    table = {}

    last_action = {
        'vertical': 0,  # last vertical action
        'horizontal': 0,  # last horizontal action
        'last_vertical': 0,  # the vertical action before last action
        'last_horizontal': 0  # the horizontal action before last action
    }
    last_reward = 0

    def __init__(self, input_path, pool=None):
        # add one example function
        self.original_cpu_util = 0.6
        self.function = 'example_function'
        self.arrival_rate = self.initial_arrival_rate
        self.cpu_shares_others = self.initial_cpu_shares_others
        self.cpu_shares_per_container = self.initial_cpu_shares
        self.frequency = self.initial_frequency
        self.num_containers = 1
        self.current_state = None
        self.pool = pool  # a list of csv files for a pool of applications
        self.table = {}
        if pool:
            self.function = 'pool'
            self.data_path = input_path + pool[0]
            self.load_data()
        else:
            self.function = input_path.split('/')[-1][:-1]
            self.data_path = input_path
            self.load_data()

    # load all data from traces
    def load_data(self):
        # read four csv files at different frequencies
        print('Function name:', self.function, '| Data path:', self.data_path)
        # data_path: 'data-freq-scaling/compress_'
        for freq in list(self.frequency_dictionary.values()):
            csv_path = self.data_path + '/' + self.function + '_' + str(freq) + '.csv'
            # csv_path: 'data-freq-scaling/compress_/compress_1.csv'
            df = pd.read_csv(csv_path)
            for index, row in df.iterrows():
                tabular_item = {
                    'avg_cpu_util': row['avg_cpu_util'],
                    'slo_preservation': row['slo_preservation'],
                    'total_cpu_shares': row['total_cpu_shares'],
                    'cpu_shares_others': row['cpu_shares_others'],
                    'num_containers':row['num_containers'],
                    'arrival_rate':row['arrival_rate'],
                    'latency': row['latency']
                }
                key = (row['cpu'], row['memory'], freq)
                self.table[key] = tabular_item
        self.max_value = df.max().to_dict()
        random_key, random_value = random.choice(list(self.table.items()))
        init_cpu = random_key[0]
        init_freq = random_key[2]
        init_cpu_util = random_value['avg_cpu_util']
        self.original_cpu_util = init_cpu_util
        self.cpu_shares_per_container = init_cpu
        self.frequency = init_freq

    # get the function name
    def get_function_name(self):
        return self.function

    # return the states
    # [cpu util percentage, slo preservation percentage, cpu.shares, cpu.shares (others), # of containers, arrival rate]
    def get_rl_states(self, cpu, memory, freq=1):
        # if num_containers > MAX_NUM_CONTAINERS:
        #     return None
        value = self.table[(cpu, memory, freq)]
        max_cpu_shares_others = self.max_value['cpu_shares_others']
        max_total_cpu_shares = self.max_value['total_cpu_shares']
        max_num_containers = self.max_value['num_containers']
        max_arrival_rate  = self.max_value['arrival_rate']
        max_latency = self.max_value['latency']
        max_freq = max(self.frequency_dictionary.values())
        return [value['avg_cpu_util'], value['slo_preservation'], value['total_cpu_shares']/max_total_cpu_shares,
                value['cpu_shares_others'], value['num_containers']/max_num_containers, value['arrival_rate']/max_arrival_rate,
                value['latency']/max_latency, freq/max_freq]

    # overprovision to num of concurrent containers + 2
    def overprovision(self, function_name):
        # horizontal scaling
        scale_action = {
            'vertical': 0,
            'horizontal': int(self.initial_arrival_rate) + 2
        }
        self.num_containers += int(self.initial_arrival_rate) + 2
        states, _, _ = self.step(function_name, scale_action)
        print('Overprovisioning:',
              '[ Avg CPU utilization: {:.7f} Avg SLO preservation: {:.7f}'.format(states[0], states[1]),
              'Num of containers:', states[4],
              'CPU shares:', states[2], 'CPU shares (others):', states[3],
              'Arrival rate:', states[5], ']')

    # reset the environment by re-initializing all states and do the overprovisioning
    def reset(self, function_name):
        if function_name != self.function:
            raise KeyError

        self.arrival_rate = self.initial_arrival_rate
        self.cpu_shares_others = self.initial_cpu_shares_others
        self.num_containers = 1
        # randomly set the cpu shares for all other containers on the same server
        cpu_shares_other = 0  # single-tenant
        self.cpu_shares_others = cpu_shares_other

        # randomly set the arrival rate
        arrival_rate = 1
        self.arrival_rate = arrival_rate

        # overprovision resources to the function initially
        # self.overprovision(function_name)
        self.cpu_shares_per_container = self.initial_cpu_shares
        self.frequency = self.initial_frequency
        scale_action = {
            'vertical': random.randint(0, 5),
            'horizontal': 0,
            'frequency': random.randint(0, 4)
        }
        # self.num_containers += 2

        states, _, _ = self.step(function_name, scale_action)

        self.current_state = self.get_rl_states(self.cpu_shares_per_container, self.memory, self.frequency)

        return self.current_state

    # step function to update the environment given the input actions
    def step(self, function_name, action):
        if function_name != self.function:
            raise KeyError
        # curr_state = self.get_rl_states(self.cpu_shares_per_container, self.memory, self.frequency)
        if action['vertical'] != -1:
            self.cpu_shares_per_container = self.cpu_dictionary[action['vertical']]
        if action['frequency'] != -1:
            self.frequency = self.frequency_dictionary[action['frequency']]
        state = self.get_rl_states(self.cpu_shares_per_container, self.memory, self.frequency)
        # calculate the reward
        reward = convert_state_action_to_reward(state, action, self.last_action, self.arrival_rate)
        self.last_reward = reward
        # self.last_action = action
        self.last_action['last_vertical'] = self.last_action['vertical']
        self.last_action['last_horizontal'] = self.last_action['horizontal']
        self.last_action['vertical'] = action['vertical']
        self.last_action['horizontal'] = action['horizontal']
        # check if done
        done = False
        self.current_state = state

        return state, reward, done

    # print function state information
    def print_info(self):
        print('Function name:', self.function)
        print('Average CPU Util:', self.current_state[0])
        print('SLO Preservation:', self.current_state[1])
        print('Total CPU Shares (normalized):', self.current_state[2])
        print('Total CPU Shares for Other Containers (normalized):', self.current_state[3])
        print('Number of Containers:', self.current_state[4] * 20)
        print('Arrival Rate (rps):', self.current_state[5] * 10)
        print('Current CPU Frequency:', self.current_state[7] * max(self.frequency_dictionary.values()))
        print('Latency:', self.current_state[6] * self.max_value['latency'])


if __name__ == '__main__':
    print('Testing simulated environment...')
    env = SimEnvironment('../data-freq-scaling/compress_')
    env.load_data()
    env.reset('compress')
    env.print_info()
