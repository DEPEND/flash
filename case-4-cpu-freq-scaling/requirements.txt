gym==0.26.2
matplotlib==3.8.3
numpy==1.26.4
pandas==2.2.1
psutil==5.9.8
torch==2.2.1
tqdm==4.65.0
transformers==4.39.1
