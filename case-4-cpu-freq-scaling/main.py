import argparse
import random
from serverless_env import SimEnvironment
from ppo import PPO
import os
from tqdm import tqdm
import numpy as np
from util import META_TRAIN_ITERATIONS, RESULTS_PATH


# How to train
# python main.py --operation=train --model_path=./model --data_path=./data/writefile_writefile_imageresize_output.csv
# How to test
# python main.py --operation=test --checkpoint_path=./model/writefile_writefile_imageresize/ppo-ep200.pth.tar --data_path=./data/writefile_writefile_imageresize_output.csv
def main(data_path, checkpoint_path, operation, model_path, verbose=False):
    """
    This is the main function for RL training and inference on an individual application.
    """
    if operation == 'train':
        if not model_path:
            print('Please specify the model directory! e.g., --model_path=./model')
            exit()
        train(data_path, model_path)
    elif operation == 'test':
        if not checkpoint_path:
            print('Please specify checkpoint path! e.g., --checkpoint_path=./model/app_name/checkpoint.pth.tar')
            exit()
        test(data_path, checkpoint_path, verbose=verbose)
    elif operation == 'adaptation':
        if not checkpoint_path:
            print('Please specify checkpoint path! e.g., --checkpoint_path=./model/app_name/checkpoint.pth.tar')
            exit()
        test(data_path, checkpoint_path, adapt=True, verbose=verbose)
    else:
        print('Unknown operation!')
        exit()


# E.g., --operation=train --model_path=./model --data_path=./data
# E.g., --operation=test --checkpoint_path=./model --data_path=./data
def mass_main(operation, data_path, checkpoint_path, model_path, pool, verbose=False):
    """
    This is the main function for mass RL training and inference on a directory of applications.
    """
    print('Data Dir:', data_path)
    print('Model Dir:', model_path)
    print('Checkpoint Path:', checkpoint_path)
    print('Operation:', operation)
    print('Pool:', pool)
    if not model_path:
        print('Please specify the model directory! e.g., --model_dir=./model')
        exit()
    # init the model folder path
    folder_path = model_path + "/pretraining-rl"
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    results_path = RESULTS_PATH  # "./results"
    if not os.path.exists(results_path):
        os.makedirs(results_path)
    # create and initialize the environment for rl training
    if operation == 'train':
        f = open(pool, 'r')
        print('Applications from', pool)
        lines = f.readlines()
        task_rewards = []
        for train_iter in tqdm(range(META_TRAIN_ITERATIONS)):
            line = random.choice(lines)
            csv_file = line.strip()
            # remove the 'output.csv' postfix
            app_name = csv_file[:-10]

            # init the RL environment with all csv_files from the application_pool
            env = SimEnvironment(data_path + '/' + app_name)
            function_name = env.get_function_name()
            print('Environment initialized for function', function_name)
            initial_state = env.reset(function_name)

            # initialize the agent and start training
            if train_iter == 0:
                agent = PPO(env, function_name, folder_path, verbose=verbose)
            else:
                agent.env = env
                agent.function_name = function_name

            task_reward = agent.train(mode='mass_train')  # set the mode to mass_train to avoid saving checkpoint for each individual function
            task_rewards.append(task_reward)
            if train_iter > 0 and train_iter % 100 == 0:
                agent.save_checkpoint(train_iter)
        agent.save_checkpoint(META_TRAIN_ITERATIONS)

        with open(results_path + '/train_rewards_rl.txt', 'w') as f:
            for reward in task_rewards:
                f.write(str(reward) + '\n')
            f.write(str(np.mean(task_rewards)) + '\n')
        print('Average training reward: ', np.mean(task_rewards))
    elif operation == 'test':
        if not checkpoint_path:
            print('Please specify checkpoint path! e.g., --checkpoint_path=./model/app_name/checkpoint.pth.tar')
            exit()
        task_rewards = []
        f = open(pool, 'r')
        print('Applications from', pool)
        lines = f.readlines()
        for line in tqdm(lines):
            csv_file = line.strip()
            # remove the 'output.csv' postfix
            app_name = csv_file[:-10]
            print('Testing for', app_name)
            task_reward = test(data_path + '/' + app_name, checkpoint_path, adapt=False)
            task_rewards.append(task_reward)
        with open(results_path + '/test_rewards_rl.txt', 'w') as f:
            for reward in task_rewards:
                f.write(str(reward) + '\n')
            f.write(str(np.mean(task_rewards)) + '\n')
        print('Average test reward: ', np.mean(task_rewards))
    elif operation == 'adaptation':
        if not checkpoint_path:
            print('Please specify checkpoint path! e.g., --checkpoint_path=./model/app_name/checkpoint.pth.tar')
            exit()
        task_rewards = []
        f = open(pool, 'r')
        print('Applications from', pool)
        lines = f.readlines()
        for line in tqdm(lines):
            csv_file = line.strip()
            # remove the 'output.csv' postfix
            app_name = csv_file[:-10]
            print('Adaptation for', app_name)
            task_reward = test(data_path + '/' + app_name, checkpoint_path, adapt=True, mode='mass_train')
            task_rewards.append(task_reward)
        with open(results_path + '/adapt_rewards_rl.txt', 'w') as f:
            for reward in task_rewards:
                f.write(str(reward) + '\n')
            f.write(str(np.mean(task_rewards)) + '\n')
        print('Average adaptation reward: ', np.mean(task_rewards))


# training
# e.g., data_path=./data/writefile_writefile_imageresize_
# e.g., model_path=./model
def train(data_path, model_path):
    # create and initialize the environment for rl training
    env = SimEnvironment(data_path)
    function_name = env.get_function_name()
    print('Environment initialized for function', function_name)
    initial_state = env.reset(function_name)
    print('Initial state:', initial_state)

    # initialize the model folder path for the particular function
    folder_path = model_path + "/" + str(function_name)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    # initialize the agent and start training
    agent = PPO(env, function_name, folder_path)
    task_reward = agent.train()
    return task_reward


# testing
# e.g., data_path=./data/writefile_writefile_imageresize_
# e.g., checkpoint_path=./model/writefile_writefile_imageresize/ppo-ep200.pth.tar
def test(data_path, checkpoint_path, adapt=False, verbose=False, mode='train'):
    env = SimEnvironment(data_path)
    function_name = env.get_function_name()
    print('Environment initialized for function', function_name)
    initial_state = env.reset(function_name)
    print('Initial state:', initial_state)
    folder_path = os.path.dirname(checkpoint_path)
    # init an RL agent
    agent = PPO(env, function_name, folder_path, verbose=verbose)
    print('RL agent initialized!')
    checkpoint_file = checkpoint_path
    agent.load_checkpoint(checkpoint_file)
    if adapt:
        task_reward = agent.train(mode=mode)
    else:
        task_reward = agent.test()
    return task_reward


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='PPO Agent Training in Autoscaling Tasks.')
    parser.add_argument('--operation', choices=['train', 'test', 'adaptation'], required=True, help="Mode in either train or test.")
    parser.add_argument('--data_path', type=str, required=True, help='Path to the data file or directory.')
    parser.add_argument('--pool', type=str, required=False, help="Training or testing on a pool of applications, e.g., --pool ./training_pool.txt")
    parser.add_argument('--checkpoint_path', type=str, required=False, help='Path to the checkpoint file.')
    parser.add_argument('--model_dir', type=str, required=False, help='Path to the model directory to store to.')
    parser.add_argument('-v', '--verbose', help="Increase output verbosity", action="store_true")
    args = parser.parse_args()

    if args.pool:
        print('Training/testing on a pool of applications:', args.pool)
        mass_main(args.operation, args.data_path, args.checkpoint_path, args.model_dir, args.pool, verbose=args.verbose)
    else:
        main(args.data_path, args.checkpoint_path, args.operation, args.model_dir, verbose=args.verbose)