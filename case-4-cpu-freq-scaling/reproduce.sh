#!/bin/bash
pool_ids=(1)
for adaptation_pool_id in "${pool_ids[@]}"; do
    # adaptation_pool_id=0
    printf "Evaluation for adaptation pool $adaptation_pool_id\n"
    export POOL_ID_ENV="$adaptation_pool_id"

    # FLASH + SmartOC agent
    printf "\n>>> FLASH + SmartOC Agent\n"
    printf "\nAdaptation:\n"
    python3 meta_main.py --operation=adaptation --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-freq-scaling --checkpoint_path=./model/pretraining-metarl/metappo-rnn.pth.tar

    printf "\nTesting (No fine-tuning):\n"
    python3 meta_main.py --operation=test --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-freq-scaling --checkpoint_path=./model/pretraining-metarl/metappo-rnn.pth.tar

    # SmartOC agent
    printf "\n>>> SmartOC Agent (No Meta-Learning)\n"
    printf "\nAdaptation:\n"
    python3 main.py --operation=adaptation --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-freq-scaling --checkpoint_path=./model/pretraining-rl/ppo.pth.tar

    printf "\nTesting (No fine-tuning):\n"
    python3 main.py --operation=test --model_dir=./model --pool=./adaptation_pool_${adaptation_pool_id}.txt --data_path=../data-freq-scaling --checkpoint_path=./model/pretraining-rl/ppo.pth.tar
done

printf "\nDone!\n"

python3 report.py