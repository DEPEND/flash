import pandas as pd
import os
import glob

# check if contains nan
# df = pd.read_csv('processed-data.csv')
# df.isnull().values.any()
  
# use glob to get all the csv files
dir_path = 'raw-data'
print('Path:', dir_path)
csv_files = glob.glob(os.path.join(dir_path, '*.csv'))

num_files = len(csv_files)
print('Total num of apps:', num_files)

# check if configs are complete
for csv_file in csv_files:
  # print('File location:', csv_file)
  missing_keys = []
  df = pd.read_csv(csv_file)
  num_configs = df.groupby(['cpu', 'memory']).ngroups
  keys = list((df.groupby(['cpu', 'memory']).groups.keys()))
  for i in [128, 256, 512, 1024, 1536, 2048]:
    for j in [128, 256, 512, 1024, 1536, 2048]:
      if (i, j) not in keys:
        missing_keys.append((i, j))
  # print('# of configs covered:', num_configs)
  if not num_configs == 36:
    print(csv_file[csv_file.rfind('/')+1:-4])
    print('Missing keys:', missing_keys)

print('Check completed!')
