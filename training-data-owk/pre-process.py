import pandas as pd
import os
import glob
import numpy as np
  
  
# use glob to get all the csv files
dir_path = 'raw-data'
print('Path:', dir_path)
csv_files = glob.glob(os.path.join(dir_path, '*.csv'))

num_files = len(csv_files)
print('Total num of apps:', num_files)

# get the column names
df = pd.read_csv(csv_files[0])
names = list(df.columns)[2:]
# append the std columns
cols = ['f_name', 'cpu', 'memory']
for name in names[2:]:
  cols.append(name + '_mean')
for name in names[2:]:
  cols.append(name + '_std')
print(cols)

final_df = pd.DataFrame(columns=cols)

# start processing
for csv_file in csv_files:
  # print('File location:', csv_file)
  df = pd.read_csv(csv_file)

  f_name = csv_file[csv_file.find("/")+1:-4]
  # print(f_name)

  res = df.groupby(['cpu', 'memory']).agg([np.mean, np.std])
  for cpu in [128, 256, 512, 1024, 1536, 2048]:
    for mem in [128, 256, 512, 1024, 1536, 2048]:
      dict_res = {'f_name': f_name, 'cpu': cpu, 'memory': mem}
      for i in res:
        field = str(i[0]) + '_' + str(i[1])
        # print(field)
        for j in res[i].index:
          if cpu == j[0] and mem == j[1]:
            # print('cpu:', cpu, 'mem:', mem)
            dict_res[field] = res[i][j]
      # print(dict_res)
      # insert the row to the final dataframe
      final_df.loc[len(final_df)] = dict_res

  print(f_name, 'completed!')

print(final_df)
final_df.to_csv('processed-data.csv')
