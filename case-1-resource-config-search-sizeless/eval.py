import argparse
import torch
from utils import init_dataset
from train import init_model, test
from utils import *
import os


parser = argparse.ArgumentParser()
parser.add_argument('--exp', type=str, default='default')  # experiment name
parser.add_argument('--dataset', type=str, default='sizeless')
parser.add_argument('--num_cls', type=int, default=1)
parser.add_argument('--num_shots', type=int, default=2)  # number of shots in few-shot learning
parser.add_argument('--batch_size', type=int, default=1)
parser.add_argument('--rnn', action='store_true')  # to replace the SNAIL architecture with an RNN (default is false)
parser.add_argument('--sizeless', action='store_true')  # to reuse Sizeless architecture (default is false)
parser.add_argument('--skip_embedding', action='store_true')  # no embedding (fall back to fully-connected NN, default is false)
parser.add_argument('--cuda', action='store_true')
opt = parser.parse_args()

opt.exp = 'experiments/exp-' + opt.exp

# if opt.skip_embedding:
#     opt.num_shots = 1  # Sizeless is 1-shot regression

device = torch.device("cuda:0" if opt.cuda else "cpu")

model = init_model(opt, device)

best_model_path = os.path.join(opt.exp, 'best_model.pth')
print('Loading the best model from:', best_model_path)
weights = torch.load(best_model_path)
model.load_state_dict(weights)
model = model.cuda() if opt.cuda else model

val_dataloader = init_dataset(opt, validation_only=True)
print('Dataset initialized!')
print('Testing with best model..')
test(opt=opt, test_dataloader=val_dataloader, model=model, device=device)