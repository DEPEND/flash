import numpy as np

# Open the file and read the column data into a numpy array
with open('inference_sizeless.txt', 'r') as f:
    column_data = np.loadtxt(f, delimiter='\t', usecols=[0])

# Calculate the mean and standard deviation of the column data
mean = np.mean(column_data)
std = np.std(column_data)

# Print the results
print(f"Mean: {mean:.2f}")
print(f"Standard deviation: {std:.2f}")
