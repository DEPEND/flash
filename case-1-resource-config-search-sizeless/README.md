# Case Study 1: Resource Config Search for Sizeless (Middleware 2021)

Category: Meta-learning for Supervised Learning in Regression Tasks

## Sizeless Dataset

Location: `../training-data-sizeless/processed_training_data.csv`

This dataset consists of the runtime resource metrics and appilcation metrics for 2000 serverless functions deployed on AWS Lambda.
There are 6 memory configurations (128, 256, 512, 1024, 1536, 3008 MB).
This amounts to 12,000 performance measurements, 120,000 minutes of experiment time, 216,000,000 Lambda executions, and roughly $2,000 worth of Lambda compute time.

## Requirement

- Python 3.8.8
- Packages listed in `requirements.txt`

## Training

To start training:

```
# embedding + fully-connected neural network -> latency
python3 train.py --dataset=sizeless                # default: 2-shots/samples
python3 train.py --dataset=sizeless --num_shots=6  # 6-shots/samples

# embedding + sizeless neural network architecture -> latency
python3 train.py --dataset=sizeless --sizeless --decay=0.01

# embedding + fully-connected neural network (with L2 regularization) -> latency
python3 train.py --dataset=sizeless --decay=0.01

# embedding + sizeless neural network architecture (without L2 regularization) -> latency
python3 train.py --dataset=sizeless --sizeless

# RNN embedding (with 4 samples) + fully-connected neural network -> latency
python3 train.py --dataset=sizeless --rnn --num_shots=4
```

## Evaluation

To evaluated the trained model on a new dataset with the same other flags used in the training:

```
# use --exp to specify the folder where the trained model is located at
python3 eval.py [--dataset=sizeless --rnn --num_shots=4] --exp=experiment-name
```

## Visualization

To visualize the training process:

```
# use --exp to specify the folder where the training loss log data is located at
python3 plot_training_loss.py --exp=experiment-name
```
