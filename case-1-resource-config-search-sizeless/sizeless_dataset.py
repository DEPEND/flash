import torch.utils.data as data
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer
import pandas as pd
import itertools
import numpy as np
from params import *
import math


# functions with missing measurements or malfunctioning
excludes = ['generated-560-white-frog', 'generated-302-cool-feather', 'json2yaml-decompress-readfile', 'generated-4-weathered-wind', 'decompress-json2yaml-json2yaml',
            'generated-234-old-sun', 'generated-476-young-silence', 'generated-389-dry-bush', 'generated-441-shy-wave', 'generated-712-white-glade',
            'readfile-json2yaml-json2yaml', 'generated-235-silent-moon', 'decompress-json2yaml-readfile', 'generated-82-weathered-tree', 'generated-795-red-snow',
            'generated-59-blue-moon', 'generated-875-polished-shadow', 'json2yaml-readfile-decompress', 'generated-781-lively-wind',
            'json2yaml-floatoperations-floatoperations', 'generated-820-hidden-water', 'floatoperations-readfile-floatoperations', 'generated-527-restless-frost',
            'generated-696-frosty-pine', 'generated-699-bitter-cherry', 'generated-174-polished-snowflake', 'generated-786-misty-cloud', 'generated-709-empty-paper']
# variables with missing measurements or malfunctioning
excludes_features = []

used_features = ['f_size', 'userDiff_mean', 'sysDiff_mean', 'heapUsed_mean', 'rel_vContextSwitches_mean', 'rel_userdiff_mean', 'rel_sysdiff_mean', 'rel_fsWrite_mean',  'rel_netByRx_mean', 'heapUsed_cov', 'mallocMem_cov', 'rel_userdiff_cov']
# all features with fields containing NaN values removed
if USE_ALL_FEATURES:
    used_features = 'maxRss_mean,fsRead_mean,fsWrite_mean,vContextSwitches_mean,ivContextSwitches_mean,userDiff_mean,sysDiff_mean,rss_mean,heapTotal_mean,heapUsed_mean,external_mean,elMin_mean,elMax_mean,elMean_mean,elStd_mean,bytecodeMetadataSize_mean,heapPhysical_mean,heapAvailable_mean,heapLimit_mean,mallocMem_mean,netByRx_mean,netPkgRx_mean,netByTx_mean,netPkgTx_mean,rel_vContextSwitches_mean,rel_userdiff_mean,rel_sysdiff_mean,rel_rss_mean,rel_heapUsed_mean,rel_heapTotal_mean,rel_external_mean,rel_fsRead_mean,rel_fsWrite_mean,rel_elMin_mean,rel_elMax_mean,rel_elMean_mean,rel_heapPhysical_mean,rel_heapAvailable_mean,rel_bytecodeMetadataSize_mean,rel_mallocMem_mean,rel_netByRx_mean,rel_netPkgRx_mean,rel_netByTx_mean,rel_netPkgTx_mean,duration_cov,maxRss_cov,vContextSwitches_cov,ivContextSwitches_cov,userDiff_cov,sysDiff_cov,rss_cov,heapTotal_cov,heapUsed_cov,external_cov,elMin_cov,bytecodeMetadataSize_cov,heapPhysical_cov,heapAvailable_cov,mallocMem_cov,netByRx_cov,netPkgRx_cov,netByTx_cov,netPkgTx_cov,rel_vContextSwitches_cov,rel_userdiff_cov,rel_sysdiff_cov,rel_rss_cov,rel_heapUsed_cov,rel_heapTotal_cov,rel_external_cov,rel_elMin_cov,rel_heapPhysical_cov,rel_heapAvailable_cov,rel_bytecodeMetadataSize_cov,rel_mallocMem_cov,rel_netByRx_cov,rel_netPkgRx_cov,rel_netByTx_cov,rel_netPkgTx_cov,f_size'.split(',')

memory_configs = ['128', '256', '512', '1024', '2048', '3008']

# normalize the latency value to [0, 1]; the base is the latency under 128 MB memory allocation
def normalize_latency_X_samples(dataset, num_samples):
    dataset['duration_to_predict'] = dataset['duration_to_predict'] / dataset['duration_s1']  # duration_128

    for i in reversed(range(num_samples)):
        if 'duration_s' + str(i + 1) in dataset.columns:
            dataset['duration_s' + str(i + 1)] = dataset['duration_s' + str(i + 1)] / dataset['duration_s1']
    return dataset

# normalize the configs to [0, 1]; the base is 3008 MB
def normalize_function_memory_config(dataset, num_samples, min_config=128.0, max_config=3008.0):
    for i in range(num_samples):
        if 'f_size_s' + str(i + 1) in dataset.columns:
            dataset['f_size_s' + str(i + 1)] /= max_config
    dataset['size_to_predict'] /= max_config
    return dataset

# extended the sizeless dataset (Middleware 2021) that supports X-sample dataset generation with separated applications
# for training and validation to test if the learned model is generalizable or adaptable to new applications
class SizelessDatasetXSamplesDifferentAppsForTrainTestValidate(data.Dataset):
    def __init__(self, num_samples, mode='train', root='../training-data-sizeless', transform=None, target_transform=None):
        '''
        Args:
        - num_sammples: number of samples used as features, remaining ones are used for prediction
        - mode: one of ['train', 'test']
        - root: the directory where the dataset is stored
        - transform: how to transform the input
        - target_transform: how to transform the target
        '''
        super(SizelessDatasetXSamplesDifferentAppsForTrainTestValidate, self).__init__()
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.mode = mode

        if num_samples > 5:
            print("Error! Maximum number of samples to use in the Sizeless dataset is 5!")
            exit()

        training_data_df = pd.read_csv(self.root + '/processed_training_data.csv').reset_index(drop=True)

        # drop malfunctioned measurements
        self.training_data_df = training_data_df[~training_data_df.f_name.isin(excludes)]
        self.training_data_df = self.training_data_df.drop(['fsRead_cov', 'fsWrite_cov', 'heapLimit_cov', 'rel_fsRead_cov', 'rel_fsWrite_cov'], axis=1)

        # group by function name
        training_data_df_grouped = self.training_data_df.groupby('f_name')

        all_cols = []

        # build the sample cols
        for i in range(num_samples):
            for col_name in used_features:
                col_name += '_s' + str(i + 1)
                all_cols.append(col_name)
            all_cols.append('duration_s' + str(i + 1))  # memory_configs[i]
        
        # build the target cols
        all_cols.append('size_to_predict')
        all_cols.append('duration_to_predict')
        print('Total number of columns:', len(all_cols))

        # init a new dataframe
        processed_df = pd.DataFrame(columns=all_cols)

        count_functions = 0
        for group_name, df_group in training_data_df_grouped:
            # print('f_name:', group_name)
            count = 0
            row_to_add = []
            for row_index, row in df_group.sort_values(['f_size']).iterrows():
                if num_samples <= count:
                    # samples to predict
                    processed_df.loc[len(processed_df.index)] = row_to_add + [int(memory_configs[count]), row['y_' + memory_configs[count]]]
                else:
                    # samples to be used as features
                    row_to_add.extend(row[used_features])
                    row_to_add.append(row['y_' + memory_configs[count]])
                    # check if nan exists
                    if any(np.isnan(row_to_add)):
                        print('Measurements data contains NaN:', group_name)
                        exit()
                count += 1
            count_functions += 1
            # if count_functions >= 10:
            #     break

        print('Total # of functions for training and validation (7:3):', count_functions)

        # normalize latency values and function configs
        self.training_data_df = normalize_latency_X_samples(processed_df, num_samples)
        self.training_data_df = normalize_function_memory_config(self.training_data_df, num_samples)

        # split the labels Y
        self.y = [elem for elem in self.training_data_df['duration_to_predict']]
        training_dataset_end_index = math.ceil(0.7 * count_functions) * (TOTAL_NUM_CONFIGS - num_samples)
        self.y_train = self.y[:training_dataset_end_index]
        self.y_test = self.y[training_dataset_end_index:]

        # extract the features X
        cols_sizes = [col for col in self.training_data_df.columns if 'size' in col]
        cols_durations = [col for col in self.training_data_df.columns if 'duration' in col and not 'predict' in col and not 'cov' in col]
        self.x = self.training_data_df.drop(self.training_data_df.columns.difference(all_cols[:-1]), axis=1)
        self.x_sizes = self.x[cols_sizes]
        self.x_durations = self.x[cols_durations]
        self.x_others = self.x.drop(cols_sizes + cols_durations, axis=1)

        # normalize features
        sc2 = QuantileTransformer(n_quantiles=min(1000, len(self.x_others)))
        self.x_others = sc2.fit_transform(self.x_others)
        sc = MinMaxScaler(clip=True)
        self.x_others = sc.fit_transform(self.x_others)

        self.x_sizes = self.x_sizes.to_numpy()
        self.x_durations = self.x_durations.to_numpy()
        # insert the size and duration columns to the the end of each sample feature vector in x_others
        # e.g., for 2 samples: a1, a2, a3, b1, b2, b3 | (m_a, t_a), (m_b, t_b)
        # -> a1, a2, a3, m_a, t_a, b1, b2, b3 | (m_b, t_b)
        # -> a1, a2, a3, m_a, t_a, b1, b2, b3, m_b, t_b
        for i in range(num_samples):
            self.x_others = np.insert(self.x_others, (i + 1) * (len(used_features) - 1) + 2 * i, self.x_sizes[:, i], axis=1)
            self.x_others = np.insert(self.x_others, (i + 1) * (len(used_features) - 1) + 2 * i + 1, self.x_durations[:, i], axis=1)
        self.x_sizes = np.delete(self.x_sizes, list(range(num_samples)), 1)
        self.x = np.concatenate((self.x_others, self.x_sizes), axis=1)

        # split the features X
        self.x_train = self.x[:training_dataset_end_index, :]
        self.x_test = self.x[training_dataset_end_index:, :]

    def __getitem__(self, idx):
        if self.mode == 'train':
            x = self.x_train[idx]
            if self.transform:
                x = self.transform(x)
            return x, self.y_train[idx]
        elif self.mode == 'test':
            x = self.x_test[idx]
            if self.transform:
                x = self.transform(x)
            return x, self.y_test[idx]

    def __len__(self):
        if self.mode == 'train':
            return len(self.y_train)
        elif self.mode =='test':
            return len(self.y_test)


if __name__ == '__main__':
    # testing
    train_dataset = SizelessDatasetXSamplesDifferentAppsForTrainTestValidate(4, mode = 'train')