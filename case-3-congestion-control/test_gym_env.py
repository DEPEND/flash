import argparse
import os
import time
import inspect
from network_simulator.pcc.aurora.schedulers import UDRTrainScheduler
from trace import Trace

# python test_gym_env.py --seed 20 --validation udr --config-file config/udr.json


def parse_args():
    """Parse arguments from the command line."""
    parser = argparse.ArgumentParser("Env testing code.")
    parser.add_argument("--seed", type=int, default=20, help="seed")
    parser.add_argument(
        "--validation",
        action="store_true",
        help="specify to enable validation.",
    )
    subparsers = parser.add_subparsers(dest="curriculum", help="CL parsers.")
    udr_parser = subparsers.add_parser("udr", help="udr")
    udr_parser.add_argument(
        "--real-trace-prob",
        type=float,
        default=0.0,
        help="Probability of picking a real trace in training",
    )
    udr_parser.add_argument(
        "--train-trace-file",
        type=str,
        default="",
        help="A file contains a list of paths to the training traces.",
    )
    udr_parser.add_argument(
        "--val-trace-file",
        type=str,
        default="",
        help="A file contains a list of paths to the validation traces.",
    )
    udr_parser.add_argument(
        "--config-file",
        type=str,
        default="",
        help="A json file which contains a list of randomization ranges with "
        "their probabilites.",
    )
    parser.add_argument(
        "--dataset",
        type=str,
        default="pantheon",
        choices=("pantheon", "synthetic"),
        help="dataset name",
    )

    return parser.parse_args()


def main():
    args = parse_args()

    # construct training_traces, validation_traces, and train_scheduler
    training_traces = []
    val_traces = []
    if args.curriculum == "udr":
        config_file = args.config_file
        if args.train_trace_file:
            with open(args.train_trace_file, "r") as f:
                for line in f:
                    line = line.strip()
                    training_traces.append(Trace.load_from_file(line))

        if args.validation and args.val_trace_file:
            with open(args.val_trace_file, "r") as f:
                for line in f:
                    line = line.strip()
                    if args.dataset == "pantheon":
                        queue = 100  # dummy value
                        val_traces.append(
                            Trace.load_from_pantheon_file(
                                line, queue=queue, loss=0
                            )
                        )
                    elif args.dataset == "synthetic":
                        val_traces.append(Trace.load_from_file(line))
                    else:
                        raise ValueError
        train_scheduler = UDRTrainScheduler(
            config_file,
            training_traces,
            percent=args.real_trace_prob,
        )
    else:
        raise NotImplementedError

    # test the environment
    from network_simulator.pcc.aurora.aurora import Aurora
    import gym
    env = gym.make('AuroraEnv-v0', trace_scheduler=train_scheduler)
    env.seed(args.seed)
    obs = env.reset()
    print("The initial observation is {}".format(obs), type(obs))
    # The initial observation is [0. 1. 1. 0. 1. 1. ... 1. 0.] <class 'numpy.ndarray'>

    # Sample a random action from the entire action space
    random_action = env.action_space.sample()
    print("Random action is {}".format(random_action.reshape(-1)), type(random_action))
    # Random action is [-5.513537e+11] <class 'numpy.ndarray'>

    # Take the action and get the new observation space
    new_obs, reward, done, info = env.step(random_action)
    print("The new observation is {}".format(new_obs.reshape(-1)))
    print(reward, done, info)

    # for i in inspect.getmembers(env):
    # # ignores anything starting with underscore (i.e., private and protected attributes)
    #     if not i[0].startswith('_'):
    #         # ignores methods
    #         if not inspect.ismethod(i[1]):
    #             print('Attribute:', i)
    #         else:
    #             print('Method:', i)
    obs_space = env.observation_space
    action_space = env.action_space
    print("The observation space: {}".format(obs_space))
    print(obs_space.shape)
    print("The action space: {}".format(action_space))
    print(action_space.shape)

if __name__ == "__main__":
    main()
