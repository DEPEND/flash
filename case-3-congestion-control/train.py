import argparse
import os
import time
import warnings
warnings.filterwarnings("ignore")

from network_simulator.pcc.aurora.aurora import Aurora
from network_simulator.pcc.aurora.schedulers import UDRTrainScheduler
from trace import Trace
from utils import set_seed, save_args

# from mpi4py.MPI import COMM_WORLD


def parse_args():
    """Parse arguments from the command line."""
    parser = argparse.ArgumentParser("RL training code.")
    parser.add_argument(
        "--exp-name", type=str, default="", help="Experiment name."
    )
    parser.add_argument(
        "--save-dir",
        type=str,
        required=True,
        help="direcotry to save the model.",
    )
    parser.add_argument("--seed", type=int, default=20, help="seed")
    parser.add_argument(
        "--total-timesteps",
        type=int,
        default=100,
        help="Total number of steps to be trained.",
    )
    parser.add_argument(
        "--pretrained-model-path",
        type=str,
        default="",
        help="Path to a pretrained model checkpoint!",
    )
    parser.add_argument(
        "--validation",
        action="store_true",
        help="specify to enable validation.",
    )
    parser.add_argument(
        "--val-freq",
        type=int,
        default=7200,
        help="specify to enable validation.",
    )
    parser.add_argument(
        "--meta-learning",
        action="store_true",
        help="specify to enable meta-learning (default False).",
    )
    subparsers = parser.add_subparsers(dest="curriculum", help="CL parsers.")
    udr_parser = subparsers.add_parser("udr", help="udr")
    udr_parser.add_argument(
        "--real-trace-prob",
        type=float,
        default=0.0,
        help="Probability of picking a real trace in training",
    )
    udr_parser.add_argument(
        "--train-trace-file",
        type=str,
        default="",
        help="A file contains a list of paths to the training traces.",
    )
    udr_parser.add_argument(
        "--val-trace-file",
        type=str,
        default="",
        help="A file contains a list of paths to the validation traces.",
    )
    udr_parser.add_argument(
        "--config-file",
        type=str,
        default="",
        help="A json file which contains a list of randomization ranges with "
        "their probabilites.",
    )
    udr_parser.add_argument(
        "--validation-config-file",
        type=str,
        default="",
        help="A json file which contains a list of randomization ranges with "
        "their probabilites.",
    )
    parser.add_argument(
        "--dataset",
        type=str,
        default="pantheon",
        choices=("pantheon", "synthetic"),
        help="dataset name",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    assert (
        not args.pretrained_model_path
        or args.pretrained_model_path.endswith(".ckpt")
    )
    os.makedirs(args.save_dir, exist_ok=True)
    save_args(args, args.save_dir)
    set_seed(args.seed)
    nprocs = 1

    # construct training_traces, validation_traces, and train_scheduler
    training_traces = []
    val_traces = []
    if args.curriculum == "udr":
        config_file = args.config_file
        validation_config_file = None
        if args.train_trace_file:
            with open(args.train_trace_file, "r") as f:
                for line in f:
                    line = line.strip()
                    training_traces.append(Trace.load_from_file(line))
            print('Loaded traces for training from:', args.train_trace_file)

        if args.validation:
            # using validation traces
            if args.val_trace_file:
                with open(args.val_trace_file, "r") as f:
                    for line in f:
                        line = line.strip()
                        if args.dataset == "pantheon":
                            queue = 100  # dummy value
                            val_traces.append(
                                Trace.load_from_pantheon_file(
                                    line, queue=queue, loss=0
                                )
                            )
                        elif args.dataset == "synthetic":
                            val_traces.append(Trace.load_from_file(line))
                        else:
                            raise ValueError
                print('Loaded traces for validation from:', args.val_trace_file)
            # using config file to generate traces for validation
            elif args.validation_config_file:
                validation_config_file = args.validation_config_file

        train_scheduler = UDRTrainScheduler(
            config_file,
            training_traces,
            percent=args.real_trace_prob,
        )
        print('RL training scheduler created with config file', config_file, 'and', len(training_traces), 'traces with probability', args.real_trace_prob)
    else:
        raise NotImplementedError

    # initialize model and agent policy
    aurora = Aurora(
        args.seed,
        args.save_dir,
        int(args.val_freq / nprocs),
        args.pretrained_model_path,
        enable_meta=args.meta_learning
    )

    # model training
    aurora.train(
        config_file,
        args.total_timesteps,
        train_scheduler,
        validation_traces=val_traces,
        validation_config_file=validation_config_file
    )


if __name__ == "__main__":
    t_start = time.time()
    main()
    print("Time used: {:.2f}s".format(time.time() - t_start))
