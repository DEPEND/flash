#!/bin/bash

python3 check_virtual_env.py
if [ $? -eq 0 ]; then
    echo "Please use a python virtual environment! Installation stopped."
    exit
fi

echo "Install python packages..."
pip install --upgrade pip --no-cache-dir
pip install gym==0.18.0
pip install pandas==1.1.5
pip install tqdm==4.62.2
pip install matplotlib==3.3.4