#!/bin/bash

set -e
save_dir=results
cl_name=aurora
exp_name=ppo
validation_config_file=config/${cl_name}.json
steps=720000
dataset_dir=data/synthetic_dataset

# (testing using synthetic dataset) --dataset-dir ${dataset_dir}
# (testing using config file) --config-file ${validation_config_file}
# (optional) -fast
idx=0
steps_l=(266400 475200 640800)
for seed in 10 20 30; do
    steps=${steps_l[$idx]}
    model_path=${save_dir}/${cl_name}/${exp_name}/seed_${seed}/model_step_${steps}.ckpt
    python test_trained_model.py \
        --save-dir ${save_dir}/${cl_name}/${exp_name}/seed_${seed}/evaluation_on_synthetic_data \
        --model-path ${model_path} \
        --dataset-dir ${dataset_dir} \
        --config-file ${validation_config_file}
    idx=$((idx+1))
done