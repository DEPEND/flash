#!/bin/bash

set -e
save_dir=results
cl_name=aurora
exp_name=ppo
total_step=720001
config_file=config/${cl_name}.json
validation_config_file=config/${cl_name}.json

for seed in 10 20 30; do
    python train.py \
        --exp-name ${exp_name} \
        --save-dir ${save_dir}/${cl_name}/${exp_name}/seed_${seed} \
        --seed ${seed} \
        --total-timesteps $total_step \
        --validation \
        udr \
        --config-file ${config_file} \
        --validation-config-file ${validation_config_file}
done
