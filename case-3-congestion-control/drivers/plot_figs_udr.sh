#!/bin/bash

set -e
save_dir=results
cl_name=aurora
exp_name=ppo

# --trace-file ${save_dir}/${cl_name}/${exp_name}/seed_${seed}/validation_traces/trace_xx.json
for seed in 10 20 30; do
    python plot_scripts/plot_time_series.py \
        --log-file ${save_dir}/${cl_name}/${exp_name}/seed_${seed}/aurora_simulation_log.csv \
        --save-dir ${save_dir}/${cl_name}/${exp_name}/seed_${seed}
done