#!/bin/bash

set -e
save_dir=results
cl_name=aurora
exp_name=ppo

for seed in 10 20 30; do
    python plot_scripts/plot_model_eval_with_synthetic_dataset.py \
        --save-dir ${save_dir}/${cl_name}/${exp_name}/seed_${seed}/evaluation_on_synthetic_data
done