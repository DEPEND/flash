import argparse
import os
import time
import shutil

import pandas as pd
import numpy as np

from network_simulator.pcc.aurora.aurora import test_on_traces
from synthetic_dataset import SyntheticDataset
from trace import generate_traces


def parse_args():
    """Parse arguments from the command line."""
    parser = argparse.ArgumentParser("Aurora model testing in simulator with synthetic datasets.")
    parser.add_argument('--model-path', type=str, default="", help="path to a trained model checkpoint")
    parser.add_argument('--save-dir', type=str, default="",
                        help="direcotry to testing results.")
    parser.add_argument('--dataset-dir', type=str, default="data/synthetic_dataset",
                        help="direcotry to dataset.")
    parser.add_argument(
        "--config-file",
        type=str,
        default="",
        help="configuration file for generating testing traces.",
    )
    parser.add_argument(
        "--meta-learning",
        action="store_true",
        help="specify to enable meta-learning (default False).",
    )
    parser.add_argument('--seed', type=int, default=42, help='seed')
    parser.add_argument('--nproc', type=int, default=16, help='proc cnt')
    parser.add_argument('--fast', action='store_true', help='fast reproduce')

    args, _ = parser.parse_known_args()
    return args


def main():
    args = parse_args()
    if not args.config_file:
        # load the traces from a synthetic dataset
        dataset = SyntheticDataset.load_from_dir(args.dataset_dir)
        if args.fast:
            ntraces = 50
        else:
            ntraces = len(dataset)
    else:
        # generate testing traces from the specified config file
        if args.fast:
            validation_traces = generate_traces(args.config_file, 50, duration=30)
            ntraces = 50
        else:
            validation_traces = generate_traces(args.config_file, 500, duration=30)
            ntraces = 500

    # save_dir = results/udr/exp-name/seed_10/evaluation_on_synthetic_data
    if os.path.exists(args.save_dir) and os.path.isdir(args.save_dir):
        shutil.rmtree(args.save_dir)
    save_dirs = [os.path.join(args.save_dir, 'trace_{:05d}'.format(i)) for i in range(ntraces)]
    model_path = args.model_path
    print('Model to test:', model_path)
    print('Meta-learning enabled?', args.meta_learning)
    print('Results directory to save to:', args.save_dir)
    if args.config_file:
        print('Testing dataset:', args.config_file, '(' + str(len(validation_traces)) + ' traces)')
    else:
        print('Testing dataset:', args.dataset_dir, '(' + str(len(save_dirs)) + ' traces)')

    input("Press Enter to continue...")
    if args.config_file:
        test_on_traces(model_path, validation_traces, save_dirs, args.nproc, args.seed, False, False, enable_meta=args.meta_learning)
    else:
        test_on_traces(model_path, dataset.traces, save_dirs, args.nproc, args.seed, False, False, enable_meta=args.meta_learning)

if __name__ == "__main__":
    t_start = time.time()
    main()
    print("Time used: {:.2f}s".format(time.time() - t_start))
