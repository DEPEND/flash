import torch.utils.data as data
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer
import pandas as pd
import itertools
import numpy as np
from params import *
import math


used_features = ['nodes', 'provider', 'A_family', 'A_vcpu', 'B_family', 'B_vcpu', 'C_family', 'C_type', 'C_vcpu']

# normalize the runtime value to [0, 1]; the base is 600s which is the timeout value
def normalize_runtime_X_samples(dataset, num_samples):
    if 'runtime_to_predict' in dataset.columns:
        dataset['runtime_to_predict'] = dataset['runtime_to_predict'] / 600.0

    for i in range(num_samples):
        if 'runtime_s' + str(i + 1) in dataset.columns:
            dataset['runtime_s' + str(i + 1)] = dataset['runtime_s' + str(i + 1)] / 600.0
    return dataset

# normalize the configs to [0, 1]; the base is defined as the max_config
def normalize_nodes_config(dataset, num_samples, min_config=2, max_config=4):
    for i in range(num_samples):
        if 'nodes_s' + str(i + 1) in dataset.columns:
            dataset['nodes_s' + str(i + 1)] /= max_config
    dataset['nodes'] /= max_config
    return dataset

# multicloud dataset that supports X-sample dataset generation with separated applications
# for training and validation to test if the learned model is generalizable or adaptable to new applications
# num_samples can be 1, 2, 3
class MultiCloudConfigSearchDatasetXSamples(data.Dataset):
    def __init__(self, num_samples, mode='train', to_predict='runtime', root='../training-data-multi-cloud-config-search', transform=None, target_transform=None):
        '''
        Args:
        - num_sammples: number of samples used as features, remaining ones are used for prediction
        - mode: one of ['train', 'test']
        - to_predict: one of ['runtime', 'cost']
        - root: the directory where the dataset is stored
        - transform: how to transform the input
        - target_transform: how to transform the target
        '''
        super(MultiCloudConfigSearchDatasetXSamples, self).__init__()
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.mode = mode

        if num_samples > 3:
            print("Error! Maximum number of samples to use in the MultiCloud dataset is 3!")
            exit()

        self.training_data_df = pd.read_csv(self.root + '/dataset_normalized.csv').reset_index(drop=True)

        # group by application
        training_data_df_grouped = self.training_data_df.groupby('workload')

        all_cols = []

        # build the sample cols
        for i in range(num_samples):
            for col_name in used_features:
                col_name += '_s' + str(i + 1)
                all_cols.append(col_name)
            # if to_predict == 'runtime':
            #     all_cols.append('runtime_s' + str(i + 1))
            # elif to_predict == 'cost':
            #     all_cols.append('cost_s' + str(i + 1))
            all_cols.append('cost_s' + str(i + 1))
            all_cols.append('runtime_s' + str(i + 1))

        # build the target cols
        for col_name in used_features:
            all_cols.append(col_name)
        if to_predict == 'cost':
            print('Preparing the dataset for cost prediction..')
            all_cols.append('cost_to_predict')
        elif to_predict == 'runtime':
            print('Preparing the dataset for runtime prediction..')
            all_cols.append('runtime_to_predict')
        # print(all_cols)
        print('Total number of columns:', len(all_cols))

        # init a new dataframe
        processed_df = pd.DataFrame(columns=all_cols)

        count_functions = 0
        for group_name, df_group in training_data_df_grouped:
            # print('workload:', group_name)
            count = 0
            row_to_add = []
            for row_index, row in df_group.sort_values(['nodes']).iterrows():
                if num_samples <= count:
                    # targets to predict
                    if to_predict == 'runtime':
                        processed_df.loc[len(processed_df.index)] = row_to_add + list(row[used_features]) + [row['target_runtime']]
                    elif to_predict == 'cost':
                        processed_df.loc[len(processed_df.index)] = row_to_add + list(row[used_features]) + [row['target_cost']]
                else:
                    # samples to be used as features
                    row_to_add.extend(row[used_features])
                    # if to_predict == 'runtime':
                    #     row_to_add.append(row['target_runtime'])
                    # elif to_predict == 'cost':
                    #     row_to_add.append(row['target_cost'])
                    row_to_add.append(row['target_cost'])
                    row_to_add.append(row['target_runtime'])
                    # check if nan exists
                    if any(np.isnan(row_to_add)):
                        print('Measurements data contains NaN:', group_name)
                        exit()
                count += 1
            count_functions += 1
            # if count_functions >= 5:
            #     break

        print('Total # of applications for training and validation (7:3):', count_functions)
        # print(processed_df)

        # dataset has been normalized 'training-data-multi-cloud-config-search/dataset_normalized.csv'
        # self.training_data_df = normalize_runtime_X_samples(processed_df, num_samples)
        # self.training_data_df = normalize_nodes_config(self.training_data_df, num_samples)
        self.training_data_df = processed_df

        if to_predict == 'runtime':
            self.y = [elem for elem in self.training_data_df['runtime_to_predict']]
        elif to_predict == 'cost':
            self.y = [elem for elem in self.training_data_df['cost_to_predict']]
        training_dataset_end_index = math.ceil(0.7 * count_functions) * (TOTAL_NUM_CONFIGS - num_samples)
        self.y_train = self.y[:training_dataset_end_index]
        self.y_test = self.y[training_dataset_end_index:]

        self.x = self.training_data_df.drop(self.training_data_df.columns.difference(all_cols[:-1]), axis=1)

        # normalize features
        # sc2 = QuantileTransformer(n_quantiles=min(1000, len(self.x)))
        # self.x = sc2.fit_transform(self.x)
        # sc = MinMaxScaler(clip=True)
        # self.x = sc.fit_transform(self.x)

        self.x_train = self.x.iloc[:training_dataset_end_index].to_numpy()
        self.x_test = self.x.iloc[training_dataset_end_index:].to_numpy()
        # self.x_train = self.x[:training_dataset_end_index, :]
        # self.x_test = self.x[training_dataset_end_index:, :]

        # print('Training:', len(self.x_train), 'entries')
        # print('Testing:', len(self.x_test), 'entries')

    def __getitem__(self, idx):
        if self.mode == 'train':
            x = self.x_train[idx]
            if self.transform:
                x = self.transform(x)
            return x, self.y_train[idx]
        elif self.mode == 'test':
            x = self.x_test[idx]
            if self.transform:
                x = self.transform(x)
            return x, self.y_test[idx]

    def __len__(self):
        if self.mode == 'train':
            return len(self.y_train)
        elif self.mode =='test':
            return len(self.y_test)


if __name__ == '__main__':
    # testing
    train_dataset = MultiCloudConfigSearchDatasetXSamples(2, mode='train', to_predict='runtime')
    train_dataset = MultiCloudConfigSearchDatasetXSamples(2, mode='train', to_predict='cost')